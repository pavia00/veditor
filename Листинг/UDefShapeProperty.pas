unit UDefShapeProperty;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Grids,
  Widgets;

type
  TDefShapeProperty = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    StringGrid1: TStringGrid;
    Button1: TButton;
    Button2: TButton;
    Footer: TPanel;
    Header: TPanel;
    ComboBox2: TComboBox;
    Label3: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure doUpdate;
    procedure WmInvalidate(var Message: TMessage); message WM_INVALIDATE;

    { Private declarations }
  public
    Shape:IShape;
    { Public declarations }
  end;

var
  DefShapeProperty: TDefShapeProperty;

implementation

{$R *.dfm}

function StringAsDataType(Value:String):TParameterDataType;
begin
if Value='������������' then Result:=pdtNumeric
 else if Value='���������' then Result:=pdtString;
end;

function DataTypeAsString(DataType:TParameterDataType):String;
begin
if DataType=pdtNumeric then result:='������������'
 else if DataType=pdtString then result:='���������'
end;

function StringAsProtection(Value:String):Boolean;
begin
if Value='��������' then Result:=True
 else if Value='����������' then Result:=False;
end;

function ProtectionAsString(Protection:Boolean):String;
begin
if Protection=True then result:='��������'
 else if Protection=False then result:='����������'
end;


procedure TDefShapeProperty.Button1Click(Sender: TObject);
begin
 Shape.Parameters.New(Edit1.Text,StringAsDataType(ComboBox1.Text),'��.', StringAsProtection(ComboBox2.Text));
 doUpdate;
end;

procedure TDefShapeProperty.Button2Click(Sender: TObject);
begin
 Shape.Parameters.Delete(StringGrid1.Selection.Top);
 doUpdate;
end;

procedure TDefShapeProperty.doUpdate;
var
   Msg: TMessage;
   I:Integer;
begin
  Msg.Msg := WM_INVALIDATE;
  Msg.WParam := 0;
  Msg.LParam := Longint(Self);
  Msg.Result := 0;
  for I:= 0 to Screen.FormCount - 1 do
    Screen.Forms[I].Dispatch(Msg);
end;

procedure TDefShapeProperty.FormShow(Sender: TObject);
var i:Integer;
begin
if Shape.Parameters.Count=0 then
  begin
  Edit1.Enabled:=False;
  ComboBox1.Enabled:=False;
  ComboBox2.Enabled:=False;
  end else
  begin
  Edit1.Enabled:=True;
  ComboBox1.Enabled:=True;
  ComboBox2.Enabled:=True;
  if Shape.Parameters.Count=0 then StringGrid1.RowCount:=2
    else StringGrid1.RowCount:=Shape.Parameters.Count+1;

  StringGrid1.Cells[0,0]:='��������';
  StringGrid1.Cells[1,0]:='���';
  StringGrid1.Cells[2,0]:='������';

  for i:=0 to Shape.Parameters.Count-1 do
    begin
    StringGrid1.Cells[0,i+1]:=Shape.Parameters.Item[i].NickName;
    StringGrid1.Cells[1,i+1]:=DataTypeAsString(Shape.Parameters.Item[i].DataType);
    StringGrid1.Cells[2,i+1]:=ProtectionAsString(Shape.Parameters.Item[i].Protection);

    end;
  end;
end;

procedure TDefShapeProperty.WmInvalidate(var Message: TMessage);
begin
 FormShow(Self);
end;

end.
