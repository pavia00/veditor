object FormSaveImage: TFormSaveImage
  Left = 241
  Top = 156
  Width = 870
  Height = 600
  Caption = #1057#1086#1093#1072#1085#1077#1085#1080#1077' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlConfigs: TPanel
    Left = 0
    Top = 0
    Width = 237
    Height = 561
    Align = alLeft
    AutoSize = True
    Locked = True
    ParentBackground = False
    TabOrder = 0
    object TransperentPanel: TGroupBox
      Left = 1
      Top = 211
      Width = 235
      Height = 134
      Align = alTop
      Caption = #1055#1088#1086#1079#1088#1072#1095#1085#1086#1089#1090#1100
      TabOrder = 0
      object TransperentCheck: TCheckBox
        Left = 16
        Top = 24
        Width = 153
        Height = 41
        Caption = #1057#1076#1077#1083#1072#1090#1100' '#1087#1088#1086#1079#1088#1072#1095#1085#1099#1084
        Checked = True
        State = cbChecked
        TabOrder = 0
        WordWrap = True
        OnClick = ColorBox1Change
      end
      object ColorBox1: TColorBox
        Left = 18
        Top = 80
        Width = 145
        Height = 22
        Cursor = crDrag
        DefaultColorColor = clWhite
        NoneColorColor = clWhite
        Selected = clWhite
        Style = [cbStandardColors, cbExtendedColors, cbCustomColor, cbPrettyNames]
        ItemHeight = 16
        TabOrder = 1
        OnChange = ColorBox1Change
      end
    end
    object FormatPanel: TRadioGroup
      Left = 1
      Top = 1
      Width = 235
      Height = 105
      Align = alTop
      Caption = #1060#1086#1088#1084#1072#1090' '#1094#1074#1077#1090#1072
      ItemIndex = 0
      Items.Strings = (
        '32'
        '16'
        '8'
        '1')
      TabOrder = 1
      OnClick = FormatPanelClick
    end
    object SavePanel: TPanel
      Left = 1
      Top = 345
      Width = 235
      Height = 62
      Align = alTop
      TabOrder = 2
      object btnCancel: TButton
        Left = 88
        Top = 16
        Width = 65
        Height = 25
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 0
      end
      object btnOk: TButton
        Left = 16
        Top = 16
        Width = 65
        Height = 25
        Caption = #1054#1082
        TabOrder = 1
        OnClick = btnOkClick
      end
    end
    object CompressPanel: TGroupBox
      Left = 1
      Top = 106
      Width = 235
      Height = 105
      Align = alTop
      Caption = #1057#1090#1077#1087#1077#1085#1100' '#1089#1078#1072#1090#1080#1103
      TabOrder = 3
      object lblCompress: TLabel
        Left = 72
        Top = 32
        Width = 29
        Height = 13
        Caption = '100 %'
      end
      object sbarCompress: TScrollBar
        Left = 24
        Top = 64
        Width = 121
        Height = 17
        PageSize = 0
        Position = 90
        TabOrder = 0
        OnChange = sbarCompressChange
      end
    end
  end
  object pnlPreview: TPanel
    Left = 237
    Top = 0
    Width = 617
    Height = 561
    Align = alClient
    AutoSize = True
    Locked = True
    ParentBackground = False
    TabOrder = 1
    OnCanResize = pnlPreviewCanResize
    object PaintBox1: TPaintBox
      Left = 1
      Top = 1
      Width = 615
      Height = 559
      Align = alClient
      OnPaint = PaintBox1Paint
    end
  end
end
