unit Widgets;

interface

uses Types,Messages, Classes, Variants, UVariantHelp, Graphics, Math, Geometrs;

const
  WM_INVALIDATE= WM_APP+$100;
var
  HandleRadius:Integer;

type
  T_RGB=packed record
    B,G,R:Byte;
    end;
  T_RGBA=packed record
    B, G, R, A:Byte;
    end;

  TA_Byte=array [0..65536] of Byte;
  PA_Byte=^TA_Byte;
  TA_RGB=array [0..65536] of T_RGB;
  PA_RGB=^TA_RGB;
  TA_RGBA=array [0..65536] of T_RGBA;
  PA_RGBA=^TA_RGBA;

const
  clTransperent:T_RGBA=(B:255;G:255;R:255;A:255);

function Blend(Background, Foreground:T_RGBA):T_RGBA;

type
  TImage=record
    Bitmap:TBitmap;
    Quadrilateral:TQuadrilateral;
    end;
Type
  TParameterDataType=(pdtString, pdtNumeric);
  IParameter=interface(IInterface)
    function GetValue: Variant;
    procedure SetValue(const Value: Variant);
    function GetNickName: String;
    function GetUnitStr: String;
    procedure SetNickName(const Value: String);
    procedure SetUnitStr(const Value: String);
    property NickName:String read GetNickName write SetNickName;
    property Value:Variant read GetValue write SetValue;
    function DataType:TParameterDataType;
    property UnitStr:String read GetUnitStr write SetUnitStr;
  end;
  TParameter=Class(TInterfacedObject, IParameter)
    private
    FValue:Variant;
    FNickName:String;
    FUnitStr:String;
    FProtection:Boolean;
    function GetValue: Variant;
    procedure SetValue(const Value: Variant);
    function GetNickName: String;
    function GetUnitStr: String;
    procedure SetNickName(const Value: String);
    procedure SetUnitStr(const Value: String);
    function GetProtection: Boolean;
    procedure SetProtection(const Value: Boolean);
    public
    parent:TStringList;
    constructor Create(AParent:TStringList); overload;
    destructor Destroy; overload;
    property NickName:String read GetNickName write SetNickName;
    property Value:Variant read GetValue write SetValue;
    function DataType:TParameterDataType;
    property UnitStr:String read GetUnitStr write SetUnitStr;
    property Protection:Boolean read GetProtection write SetProtection;
  end;

  IShapeParameters=interface(IInterface)
    function GetValue(name: String): Variant;
    procedure SetValue(name: String; const Value: Variant);
    function GetItem(Index: Integer): TParameter;
    procedure SetItem(Index: Integer; const Value: TParameter);
    function GetCount: Integer;
    procedure SetCount(const Value: Integer);
    property Count:Integer read GetCount write SetCount;
    property Value[name:String]:Variant read GetValue write SetValue;
    property Item[index:Integer]:TParameter read GetItem write SetItem;
    procedure New(NickName:String; DataType:TParameterDataType; UnitStr:String; Protection:Boolean);
    procedure Delete(Index:Integer);
  end;

  TShapeParameters=Class(TInterfacedObject, IShapeParameters)
    private
    FData:TStringList;
    function GetValue(name: String): Variant;
    procedure SetValue(name: String; const Value: Variant);
    function GetItem(Index: Integer): TParameter;
    procedure SetItem(Index: Integer; const Value: TParameter);
    function GetCount: Integer;
    procedure SetCount(const Value: Integer);
    public
    constructor Create; overload;
    destructor Destroy; overload;
    property Count:Integer read GetCount write SetCount;
    property Value[name:String]:Variant read GetValue write SetValue;
    property Item[index:Integer]:TParameter read GetItem write SetItem;
    procedure New(NickName:String; DataType:TParameterDataType; UnitStr:String; Protection:Boolean);
    procedure Delete(Index:Integer);
  end;

type
  TShapeKind=(skUndefine, skLine, skPolyLine, skBezier,skImage,skGroup);
  IShape=interface(IInterface)
    function GetSelected:Boolean;
    procedure SetSelected(value:Boolean);
    procedure Draw;
    property  Selected:Boolean read GetSelected write SetSelected;
    function  Distance(Point:TPoint):Real;
    function  InRect(Rect:TRect):Boolean;
    procedure ShiftTo(Offset:TPoint);
    function  GetPixel(X,Y:Integer):T_RGBA;
    function  GetSelf:TObject;
    function  AABBRect:TRect;
    function  ShapeKind:TShapeKind;
    function  Parameters:IShapeParameters;
  end;

  TEditPoint=class;
  IEditPoint=interface(IInterface)
    function GetSelf:TEditPoint;
    function GetX:Integer;
    procedure SetX(value:Integer);
    function GetY:Integer;
    procedure SetY(value:Integer);
    function GetIndex:Integer;
    procedure SetIndex(value:Integer);
    property X:Integer read GetX write SetX;
    property Y:Integer read GetY write SetY;
    property Index:Integer read GetIndex write SetIndex;
    function Point:TPoint;
  end;

  TChangePoint= procedure (Point:IEditPoint) of object;
  TEditPoint=Class(TInterfacedObject, IEditPoint)
    private
    FIndex:Integer;
    FPoint:TPoint;
    FOnChange:TChangePoint;
    procedure Change;
    function GetSelf:TEditPoint;
    function GetX:Integer;
    procedure SetX(value:Integer);
    function GetY:Integer;
    procedure SetY(value:Integer);
    function GetIndex:Integer;
    procedure SetIndex(value:Integer);
    public
    constructor Create; overload;
    destructor Destroy; overload;
    property OnChange: TChangePoint read FOnChange write FOnChange;
    property X:Integer read GetX write SetX;
    property Y:Integer read GetY write SetY;
    property Index:Integer read GetIndex write SetIndex;
    function Point:TPoint;
  end;

  THandleControl=class;
  TControlFrameDraw= procedure (HandleControl:THandleControl) of object;
  TControlFrameBinded= procedure (HandleControl:THandleControl) of object;
  THandleControl= class
    private
    FOnDraw:TControlFrameDraw;
    FOnBinded:TControlFrameBinded;
    FVisible:Boolean;
    FPoints:array of TEDitPoint;
    FShape:TObject;
    FEditPoint:IEditPoint;
    function  GetVisible:Boolean;
    procedure SetVisible(value:Boolean);
    procedure LineChange(APoint:IEditPoint);
    procedure BezierChange(APoint:IEditPoint);
    procedure ImageChange(APoint:IEditPoint);
    procedure GroupChange(APoint:IEditPoint);
    function ShapeKind:TShapeKind;
    public
    procedure  Binded(Shape:IShape);
    constructor Create; overload;
    destructor Destroy; overload;
    procedure Draw;
    function  IntersectHandle(Point:TPoint):Boolean;
    function  HintPoint(Point:TPoint):Boolean;
    property  OnDraw: TControlFrameDraw read FOnDraw write FOnDraw;
    property  OnBinded: TControlFrameBinded read FOnBinded write FOnBinded;
    property  Visible:Boolean read GetVisible write SetVisible;
    function  SelectedPoint:IEditPoint;
    function  Shape:IShape;
    procedure MulMatrix(M:TMatrix33);
  end;

  TLineControl=class;
  TLineDraw= procedure (LineControl:TLineControl) of object;

  TLineControl=class(TInterfacedObject, IShape)
    private
    FOnDraw:TLineDraw;
    FSelected:Boolean;
    FParameters:TShapeParameters;
    function GetSelected:Boolean;
    procedure SetSelected(value:Boolean);
    function GetSelf:TObject;
    public
    Segment:TSegment;
    Pen:TPen;
    constructor Create; overload;
    constructor Create(P0,P1:TPoint; APen:TPen); overload;
    destructor Destroy; override;
    procedure Draw;
    function  Distance(Point:TPoint):Real;
    function  InRect(Rect:TRect):Boolean;
    procedure ShiftTo(Offset:TPoint);
    function  GetPixel(X,Y:Integer):T_RGBA;
    function  AABBRect:TRect;
    property  OnDraw: TLineDraw read FOnDraw write FOnDraw;
    property  Selected:Boolean read GetSelected write SetSelected;
    function  ShapeKind:TShapeKind;
    function  Parameters:IShapeParameters;
  end;
var DefulteLineDraw:TLineDraw;

type
  TPolyLineControl=class;
  TPolyLineDraw= procedure (PolyLineControl:TPolyLineControl) of object;

  TPolyLineControl=class(TInterfacedObject, IShape)
    private
    FOnDraw:TPolyLineDraw;
    FSelected:Boolean;
    FParameters:TShapeParameters;
    function GetSelected:Boolean;
    procedure SetSelected(value:Boolean);
    function GetSelf:TObject;
    public
    PolyLine:TPolyLine;
    Pen:TPen;
    constructor Create; overload;
    constructor Create(P0,P1:TPoint; APen:TPen); overload;
    destructor Destroy; override;
    procedure Draw;
    function  Distance(Point:TPoint):Real;
    function  InRect(Rect:TRect):Boolean;
    procedure ShiftTo(Offset:TPoint);
    function  GetPixel(X,Y:Integer):T_RGBA;
    function  AABBRect:TRect;
    property  OnDraw: TPolyLineDraw read FOnDraw write FOnDraw;
    property  Selected:Boolean read GetSelected write SetSelected;
    function  ShapeKind:TShapeKind;
    function  Parameters:IShapeParameters;
  end;
var DefultePolyLineDraw:TPolyLineDraw;

type
  TBezierControl=class;
  TBezierDraw= procedure (SplineControl:TBezierControl) of object;

  TBezierControl=class(TInterfacedObject, IShape)
    private
    FOnDraw:TBezierDraw;
    FSelected:Boolean;
    FParameters:TShapeParameters;
    function GetSelected:Boolean;
    procedure SetSelected(value:Boolean);
    function GetSelf:TObject;
    public
    Spline:TSpline;
    Pen:TPen;
    constructor Create; overload;
    constructor Create(P0, P1, P2, P3:TPoint; APen:TPen); overload;
    destructor Destroy; override;
    procedure Draw;
    function  Distance(Point:TPoint):Real;
    function  InRect(Rect:TRect):Boolean;
    procedure ShiftTo(Offset:TPoint);
    function  GetPixel(X,Y:Integer):T_RGBA;
    function  AABBRect:TRect;
    property  OnDraw: TBezierDraw read FOnDraw write FOnDraw;
    property  Selected:Boolean read GetSelected write SetSelected;
    function  ShapeKind:TShapeKind;
    function  Parameters:IShapeParameters;
  end;

var DefulteSplineDraw:TBezierDraw;

type
  TRectControl=class;
  TRectDraw= procedure (RectControl:TRectControl) of object;

  TRectControl=class(TInterfacedObject, IShape)
    private
    FOnDraw:TRectDraw;
    FSelected:Boolean;
    FParameters:TShapeParameters;
    function GetSelected:Boolean;
    procedure SetSelected(value:Boolean);
    function GetSelf:TObject;
    public
    Rect:TRect;
    Brush:TBrush;
    Pen:TPen;
    constructor Create; overload;
    destructor Destroy; override;
    procedure Draw;
    function  Distance(Point:TPoint):Real;
    function  InRect(Rect:TRect):Boolean;
    procedure ShiftTo(Offset:TPoint);
    function  GetPixel(X,Y:Integer):T_RGBA;
    function  AABBRect:TRect;
    property  OnDraw: TRectDraw read FOnDraw write FOnDraw;
    property  Selected:Boolean read GetSelected write SetSelected;
    function  ShapeKind:TShapeKind;
    function  Parameters:IShapeParameters;
  end;

var DefulteRectDraw:TRectDraw;
type
  TImageControl=class;
  TImageDraw= procedure (ImageControl:TImageControl) of object;

  TImageControl=class(TInterfacedObject, IShape)
    private
    FOnDraw:TImageDraw;
    FSelected:Boolean;
    FParameters:TShapeParameters;
    function GetSelected:Boolean;
    procedure SetSelected(value:Boolean);
    function GetSelf:TObject;
    public
    Image:TImage;
    constructor create(Bitmap:TBitmap);
    destructor Destroy; override;
    procedure Draw;
    function  Distance(Point:TPoint):Real;
    function  InRect(Rect:TRect):Boolean;
    procedure ShiftTo(Offset:TPoint);
    function  GetPixel(X,Y:Integer):T_RGBA;
    function  AABBRect:TRect;
    property  OnDraw: TImageDraw read FOnDraw write FOnDraw;
    property  Selected:Boolean read GetSelected write SetSelected;
    function  ShapeKind:TShapeKind;
    function  Parameters:IShapeParameters;
  end;
var DefulteImageDraw:TImageDraw;

type
  TGroupControl=class;

  TGroup=record
    Lines:array of TLineControl;
    PolyLines:array of TPolyLineControl;
    Splines:array of TBezierControl;
    Images:array of TImageControl;
    Groups:array of TGroupControl;
    Shapes:array of IShape;
  end;

  TGroupDraw= procedure (GroupControl:TGroupControl) of object;

  TGroupControl=class(TInterfacedObject, IShape)
    private
    FOnDraw:TGroupDraw;
    FSelected:Boolean;
    FParameters:TShapeParameters;
    function GetSelected:Boolean;
    procedure SetSelected(value:Boolean);
    function GetSelf:TObject;
    public
    Group:TGroup;
    constructor Create; overload;
    destructor Destroy; override;
    procedure Draw;
    function  Distance(Point:TPoint):Real;
    function  InRect(Rect:TRect):Boolean;
    procedure ShiftTo(Offset:TPoint);
    function  GetPixel(X,Y:Integer):T_RGBA;
    function  AABBRect:TRect;
    property  OnDraw: TGroupDraw read FOnDraw write FOnDraw;
    property  Selected:Boolean read GetSelected write SetSelected;
    procedure AddLine(Line:TLineControl);
    procedure AddPolyLine(PolyLine: TPolyLineControl);
    procedure AddSpline(Spline:TBezierControl);
    procedure AddImage(Image:TImageControl);
    procedure AddGroup(aGroup: TGroupControl);
    procedure Add(value:TObject); overload;
    procedure Add(value:IShape); overload;
    procedure Swap(I,J:Integer); overload;
    procedure DeSelect;
    function  IndexBy(value:TObject):integer; overload;
    function  IndexBy(value:IShape):integer; overload;
    procedure Delete(index:Integer); overload;
    procedure DeleteAll;
    function  ShapesCount:Integer;
    procedure Resize(Rect:TRect);
    function  ShapeKind:TShapeKind;
    function  Parameters:IShapeParameters;
  end;
var DefulteGroupDraw:TGroupDraw;

implementation

function Blend(Background, Foreground:T_RGBA):T_RGBA;
begin
 // [!] �� ��������� not ��� 255-A � ��� Result.A
 Result.R := (255+Foreground.R * Foreground.A + Background.R *
                  (not Foreground.A)) shr 8;
 Result.G := (255+Foreground.G * Foreground.A + Background.G *
                  (not Foreground.G)) shr 8;
 Result.B := (255+Foreground.B * Foreground.A + Background.B *
                  (not Foreground.B)) shr 8;
 Result.A := (255+Foreground.A * Foreground.A + Background.A *
                  (not Foreground.A)) shr 8;
end;

{ TImageControl }

function TImageControl.AABBRect: TRect;
begin
  Result:=Geometrs.AABBRect(Image.Quadrilateral);
end;

constructor TImageControl.create(Bitmap: TBitmap);
begin
FParameters:=TShapeParameters.Create;
FParameters.New('����������� �����������', pdtString, '', True);
FParameters.New('����������� �����', pdtString, '', True);
FParameters.New('������������', pdtString, '', True);


FOnDraw:=DefulteImageDraw;
Image.Bitmap:=Bitmap;
Image.Quadrilateral.P0:=Point(0,0);
Image.Quadrilateral.P1:=Point(Bitmap.Width,0);
Image.Quadrilateral.P2:=Point(Bitmap.Width, Bitmap.Height);
Image.Quadrilateral.P3:=Point(0,Bitmap.Height);
end;

destructor TImageControl.Destroy;
begin
  Image.Bitmap.Free;
  inherited Destroy;
end;

function TImageControl.Distance(Point: TPoint): Real;
begin
 Result:=Geometrs.Distance(Point, Image.Quadrilateral);
end;

procedure TImageControl.Draw;
begin
if Assigned(FOnDraw) then FOnDraw(self);
end;

function TImageControl.GetPixel(X, Y: Integer): T_RGBA;
var Line:PA_RGBA;
begin
 if Distance(Point(X,Y))=0 then
    begin
    Line:=Image.Bitmap.ScanLine[Y-Self.Image.Quadrilateral.P0.Y];
    Result:=Line[X-Self.Image.Quadrilateral.P0.X];
    end else Result:=clTransperent;
end;

function TImageControl.GetSelected: Boolean;
begin
Result:=FSelected;
end;

function TImageControl.GetSelf: TObject;
begin
  result:=Self;
end;

function TImageControl.InRect(Rect: TRect): Boolean;
begin
Result:=QuadrilateralInRect(Image.Quadrilateral,Rect);
end;

function TImageControl.Parameters: IShapeParameters;
begin
  Result:=FParameters;
end;

procedure TImageControl.SetSelected(value: Boolean);
begin
FSelected:=Value;
end;

function TImageControl.ShapeKind: TShapeKind;
begin
  result:=skImage;
end;

procedure TImageControl.ShiftTo(Offset: TPoint);
begin
 Image.Quadrilateral.P0:=Add(Image.Quadrilateral.P0, Offset);
 Image.Quadrilateral.P1:=Add(Image.Quadrilateral.P1, Offset);
 Image.Quadrilateral.P2:=Add(Image.Quadrilateral.P2, Offset);
 Image.Quadrilateral.P3:=Add(Image.Quadrilateral.P3, Offset);
end;

{ TLineControl }

constructor TLineControl.Create;
begin
Pen:=TPen.Create;
OnDraw:=DefulteLineDraw;
FParameters:=TShapeParameters.Create;
FParameters.New('����������� �����������', pdtString, '', True);
FParameters.New('����������� �����', pdtString, '', True);
FParameters.New('������������', pdtString, '', True);


Self._AddRef;
end;

function TLineControl.AABBRect: TRect;
begin
  Result:=Geometrs.AABBRect(Segment);
end;

constructor TLineControl.Create(P0, P1: TPoint; APen: TPen);
begin
OnDraw:=DefulteLineDraw;
Segment.P0:=P0;
Segment.P1:=P1;
Pen:=TPen.Create;
Pen.Assign(APen);
FParameters:=TShapeParameters.Create;
FParameters.New('����������� �����������', pdtString, '', True);
FParameters.New('����������� �����', pdtString, '', True);
FParameters.New('������������', pdtString, '', True);

Self._AddRef;
end;


destructor TLineControl.Destroy;
begin
  Pen.Free;
  Self._Release;
  inherited;
end;

function TLineControl.Distance(Point: TPoint): Real;
begin
  Result:=Geometrs.Distance(Point, Segment);
end;

procedure TLineControl.Draw;
begin
if Assigned(FOnDraw) then FOnDraw(self);
end;

function TLineControl.GetPixel(X, Y: Integer): T_RGBA;
begin
 if Distance(Point(X,Y))<Pen.Width then
    begin
    Result:=T_RGBA(Pen.Color);
    Result.A:=255;
    end else Result:=clTransperent;
end;

function TLineControl.GetSelected: Boolean;
begin
Result:=FSelected;
end;

function TLineControl.GetSelf: TObject;
begin
  result:=self;
end;

function TLineControl.InRect(Rect: TRect): Boolean;
begin
 Result:=SegmentInRect(Segment,Rect);
end;

procedure TLineControl.SetSelected(value: Boolean);
begin
FSelected:=Value;
end;

procedure TLineControl.ShiftTo(Offset: TPoint);
begin
 Segment.P0:=Add(Segment.P0, Offset);
 Segment.P1:=Add(Segment.P1, Offset);
end;

function TLineControl.ShapeKind: TShapeKind;
begin
 result:=skLine;
end;

function TLineControl.Parameters: IShapeParameters;
begin
  Result:=FParameters;
end;

{ TRectControl }

function TRectControl.AABBRect: TRect;
begin
  result:=Rect;
end;

constructor TRectControl.Create;
begin
  inherited;
  Self._AddRef;
  Pen:=TPen.Create;
  OnDraw:=DefulteRectDraw;
  FParameters:=TShapeParameters.Create;
  FParameters.New('����������� �����������', pdtString, '', True);
  FParameters.New('����������� �����', pdtString, '', True);
  FParameters.New('������������', pdtString, '', True);
end;

destructor TRectControl.Destroy;
begin
  Self._Release;
  inherited;
end;

function TRectControl.Distance(Point: TPoint): Real;
begin
  Result:=Geometrs.Distance(Point, Rect);
end;

procedure TRectControl.Draw;
begin
if Assigned(FOnDraw) then FOnDraw(self);
end;

function TRectControl.GetPixel(X, Y: Integer): T_RGBA;
begin
 if Distance(Point(X,Y))<Pen.Width then
    begin
    Result:=T_RGBA(Pen.Color);
    Result.A:=255;
    end else Result:=clTransperent;
end;

function TRectControl.GetSelected: Boolean;
begin
  Result:=FSelected;
end;

function TRectControl.GetSelf: TObject;
begin
  result:=Self;
end;

function TRectControl.InRect(Rect: TRect): Boolean;
begin
  result:=RectInRect(Self.Rect, Rect);
end;

function TRectControl.Parameters: IShapeParameters;
begin
  Result:=FParameters;
end;

procedure TRectControl.SetSelected(value: Boolean);
begin
  FSelected:=Value;
end;

function TRectControl.ShapeKind: TShapeKind;
begin
  result:=skUndefine;
end;

procedure TRectControl.ShiftTo(Offset: TPoint);
begin
 Rect.TopLeft:=Add(Rect.TopLeft, Offset);
 Rect.BottomRight:=Add(Rect.BottomRight, Offset);
end;

{ TBezierControl }
constructor TBezierControl.Create;
begin
  Pen:=TPen.Create;
  OnDraw:=DefulteSplineDraw;
  FParameters:=TShapeParameters.Create;
  FParameters.New('����������� �����������', pdtString, '', True);
  FParameters.New('����������� �����', pdtString, '', True);
  FParameters.New('������������', pdtString, '', True);
  Self._AddRef;
end;

function TBezierControl.AABBRect: TRect;
var
  Bezier:TBezier;
begin
  Bezier:=SplineToBezier(Spline);
  Result:=Geometrs.AABBRect(Bezier);
end;

constructor TBezierControl.Create(P0, P1, P2, P3: TPoint; APen: TPen);
begin
  OnDraw:=DefulteSplineDraw;
  Spline.P0:=P0;
  Spline.P1:=P1;
  Spline.P2:=P2;
  Spline.P3:=P3;
  Pen:=TPen.Create;
  Pen.Assign(APen);
  FParameters:=TShapeParameters.Create;
  FParameters.New('����������� �����������', pdtString, '', True);
  FParameters.New('����������� �����', pdtString, '', True);
  FParameters.New('������������', pdtString, '', True);
  Self._AddRef;
end;

destructor TBezierControl.Destroy;
begin
  Pen.Free;
  Self._Release;
  inherited;
end;

function TBezierControl.Distance(Point: TPoint): Real;
begin
  Result:=Geometrs.Distance(Point, Spline);
end;

procedure TBezierControl.Draw;
begin
  if Assigned(FOnDraw) then FOnDraw(self);
end;

function TBezierControl.GetPixel(X, Y: Integer): T_RGBA;
begin
   if Distance(Point(X,Y))<Pen.Width then
    begin
    Result:=T_RGBA(Pen.Color);
    Result.A:=255;
    end else Result:=clTransperent;
end;

function TBezierControl.GetSelected: Boolean;
begin
  Result:=FSelected;
end;

function TBezierControl.GetSelf: TObject;
begin
  Result:=Self;
end;

function TBezierControl.InRect(Rect: TRect): Boolean;
begin
    Result:=SplineInRect(Spline,Rect);
end;

procedure TBezierControl.SetSelected(value: Boolean);
begin
  FSelected:=Value;
end;

procedure TBezierControl.ShiftTo(Offset: TPoint);
begin
  Spline.P0:=Add(Spline.P0, Offset);
  Spline.P1:=Add(Spline.P1, Offset);
  Spline.P2:=Add(Spline.P2, Offset);
  Spline.P3:=Add(Spline.P3, Offset);
end;

function TBezierControl.ShapeKind: TShapeKind;
begin
 result:=skBezier;
end;

function TBezierControl.Parameters: IShapeParameters;
begin
  Result:=FParameters;
end;

{ TGroupControl }

procedure TGroupControl.Add(value: TObject);
begin
  if value is  TImageControl then AddImage(value as TImageControl);
  if value is  TLineControl then AddLine(value as TLineControl);
  if value is  TPolyLineControl then AddPolyLine(value as TPolyLineControl);
  if value is  TBezierControl then AddSpline(value as TBezierControl);
  if value is  TGroupControl then AddGroup(value as TGroupControl);
end;

procedure TGroupControl.AddImage(Image: TImageControl);
begin
 SetLength(Group.Shapes, Length(Group.Shapes)+1);
 Group.Shapes[Length(Group.Shapes)-1]:=Image;
 SetLength(Group.Images, Length(Group.Images)+1);
 Group.Images[Length(Group.Images)-1]:=Image;
end;

procedure TGroupControl.AddLine(Line: TLineControl);
begin
 SetLength(Group.Shapes, Length(Group.Shapes)+1);
 Group.Shapes[Length(Group.Shapes)-1]:=Line;

 SetLength(Group.Lines, Length(Group.Lines)+1);
 Group.Lines[Length(Group.Lines)-1]:=Line;
end;

procedure TGroupControl.AddPolyLine(PolyLine: TPolyLineControl);
begin
 SetLength(Group.Shapes, Length(Group.Shapes)+1);
 Group.Shapes[Length(Group.Shapes)-1]:=PolyLine;

 SetLength(Group.PolyLines, Length(Group.PolyLines)+1);
 Group.PolyLines[Length(Group.PolyLines)-1]:=PolyLine;
end;

procedure TGroupControl.AddSpline(Spline: TBezierControl);
begin
 SetLength(Group.Shapes, Length(Group.Shapes)+1);
 Group.Shapes[Length(Group.Shapes)-1]:=Spline;

 SetLength(Group.Splines, Length(Group.Splines)+1);
 Group.Splines[Length(Group.Splines)-1]:=Spline;
end;

procedure TGroupControl.AddGroup(aGroup: TGroupControl);
begin
 SetLength(Group.Shapes, Length(Group.Shapes)+1);
 Group.Shapes[Length(Group.Shapes)-1]:=aGroup;

 SetLength(Group.Groups, Length(Group.Groups)+1);
 Group.Groups[Length(Group.Groups)-1]:=aGroup;
end;

function TGroupControl.Distance(Point: TPoint): Real;
var i:Integer;
  D:Real;
begin
  Result:=MaxDouble;
  for i:=0 to Length(Group.Shapes)-1 do
    begin
    D:=Group.Shapes[i].Distance(Point);
    if D< result then Result:=D;
    end;
end;

procedure TGroupControl.Draw;
begin
  if Assigned(FOnDraw) then FOnDraw(self);
end;

function TGroupControl.GetPixel(X, Y: Integer): T_RGBA;
var i:Integer;
begin
  Result:=clTransperent;
  for i:=0 to Length(Group.Shapes)-1 do
    Result:=Blend(Result,Group.Shapes[i].GetPixel(X,Y));
end;

function TGroupControl.GetSelected: Boolean;
begin
  result:=FSelected;
end;

function TGroupControl.GetSelf: TObject;
begin
  Result:=Self;
end;

function TGroupControl.InRect(Rect: TRect): Boolean;
begin
  result:=RectInRect(Self.AABBRect, Rect);
end;

procedure TGroupControl.SetSelected(value: Boolean);
begin
  FSelected:=value;
end;

procedure TGroupControl.ShiftTo(Offset: TPoint);
var i:Integer;
begin
  for i:=0 to Length(Group.Shapes)-1 do
     begin
       Group.Shapes[i].ShiftTo(Offset);
     end;
end;

procedure TGroupControl.Add(value: IShape);
begin
 Add(value.GetSelf);
end;

procedure TGroupControl.Delete(index: Integer);
var obj:TObject;
var i,j:Integer;
begin
obj:=Group.Shapes[Index].GetSelf;

  j:=0;
  for i:=0 to Length(Group.Lines)-1 do
    begin
    if Group.Lines[i] <> obj  then
     begin
     Group.Lines[j]:=Group.Lines[i];
     Inc(j);
     end else
     Group.Lines[i]:=Nil;  // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(Group.Lines, J);

  j:=0;
  for i:=0 to Length(Group.PolyLines)-1 do
    begin
    if Group.PolyLines[i] <> obj  then
     begin
     Group.PolyLines[j]:=Group.PolyLines[i];
     Inc(j);
     end else
     Group.PolyLines[i]:=Nil;  // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(Group.PolyLines, J);

  j:=0;
  for i:=0 to Length(Group.Splines)-1 do
    begin
    if Group.Splines[i] <> obj  then
     begin
     Group.Splines[j]:=Group.Splines[i];
     Inc(j);
     end else
     Group.Splines[i]:=Nil;  // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(Group.Splines, J);

  j:=0;
  for i:=0 to Length(Group.Images)-1 do
    begin
    if Group.Images[i] <> obj then
     begin
     Group.Images[j]:=Group.Images[i];
     Inc(j);
     end else Group.Images[i]:=Nil; // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(Group.Images, J);

  j:=0;
  for i:=0 to Length(Group.Groups)-1 do
    begin
    if Group.Groups[i] <> obj then
     begin
     Group.Groups[j]:=Group.Groups[i];
     Inc(j);
     end else Group.Groups[i]:=Nil; // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(Group.Groups, J);

  j:=0;
  for i:=0 to Length(Group.Shapes)-1 do
    if Group.Shapes[i].GetSelf <> obj  then
     begin
     Group.Shapes[j]:=Group.Shapes[i];
     Inc(j);
     end else Group.Shapes[i]:=nil; // ��� ������������ �������� ����� Free ����������� nil
  SetLength(Group.Shapes, J);

end;

function TGroupControl.IndexBy(value: TObject): integer;
var i:Integer;
begin
result:=-1;
for i:=0 to Length(Group.shapes)-1 do
  begin
  if value=Group.shapes[i].GetSelf then
    begin
    result:=i;
    exit;
    end;
  end;
end;

function TGroupControl.IndexBy(value: IShape): integer;
begin
IndexBy(value.GetSelf);
end;

constructor TGroupControl.Create;
begin
  OnDraw:=DefulteGroupDraw;
  Self._AddRef;
  FParameters:=TShapeParameters.Create;
end;

destructor TGroupControl.Destroy;
begin
  Self._Release;
  inherited;
end;

function TGroupControl.AABBRect: TRect;
var Rect:TRect;
 i:Integer;
begin
  if Length(Group.Shapes)=0 then
    begin
    result.Left:=0;  Result.Right:=0;
    result.Top:=0;   Result.Bottom:=0;
    end else
    begin
    Result:=Group.Shapes[0].AABBRect;
    for i:=1 to Length(Group.Shapes)-1 do
       begin
         Rect:=Group.Shapes[i].AABBRect;
         Result.Left:=Min(Result.Left, Rect.Left);
         Result.Right:=Max(Result.Right, Rect.Right);
         Result.Top:=Min(Result.Top, Rect.Top);
         Result.Bottom:=Max(Result.Bottom, Rect.Bottom);
       end;
    end;
end;

procedure TGroupControl.DeSelect;
var i:Integer;
begin
for i:=0 to Length(Group.Shapes)-1 do
  Group.Shapes[i].Selected:=False;

end;

procedure TGroupControl.DeleteAll;
begin
  SetLength(Self.Group.Lines,0);
  SetLength(Self.Group.PolyLines,0);
  SetLength(Self.Group.Splines,0);
  SetLength(Self.Group.Images,0);
  SetLength(Self.Group.Groups,0);
  SetLength(Self.Group.Shapes,0);
end;


procedure TGroupControl.Swap(I, J: Integer);
var Shape:IShape;
begin
if (I<0) or (I>=Length(Group.Shapes)) then exit;
if (J<0) or (J>=Length(Group.Shapes)) then exit;

Shape:=Group.Shapes[i];
Group.Shapes[i]:=Group.Shapes[j];
Group.Shapes[j]:=Shape;
end;

function TGroupControl.ShapesCount: Integer;
begin
  Result:=Length(Group.Shapes);
end;

procedure TGroupControl.Resize(Rect: TRect);
var
 Matrix:TMatrix33;
 T, C,S:TVectorReal;
 W1,W2,H1,H2:Real;
 HandleControl:THandleControl;
begin
  C.X:=(AABBRect.Left+AABBRect.Right)/2;
  C.Y:=(AABBRect.Top+AABBRect.Bottom)/2;
  T.X:=(Rect.Left+Rect.Right)/2;
  T.Y:=(Rect.Top+Rect.Bottom)/2;
//  T:=Sub(Rect.TopLeft,AABBRect.TopLeft);
  W1:=AABBRect.Right-AABBRect.Left;
  W2:=Rect.Right-Rect.Left;
  H1:=AABBRect.Bottom-AABBRect.Top;
  H2:=Rect.Bottom-Rect.Top;
  if W1=0 then S.X:=1 else
    S.X:=W2/W1;
  if H1=0 then S.Y:=1 else
    S.Y:=H2/H1;
  HandleControl:=THandleControl.Create;
  HandleControl.Binded(Self);
  Matrix[0,0]:=1;    Matrix[0,1]:=0;    Matrix[0,2]:=-C.X;
  Matrix[1,0]:=0;    Matrix[1,1]:=1;    Matrix[1,2]:=-C.Y;
  Matrix[2,0]:=0;    Matrix[2,1]:=0;    Matrix[2,2]:=1;
  HandleControl.MulMatrix(Matrix);
  Matrix[0,0]:=S.X;  Matrix[0,1]:=0;    Matrix[0,2]:=0;
  Matrix[1,0]:=0;    Matrix[1,1]:=S.Y;  Matrix[1,2]:=0;
  Matrix[2,0]:=0;    Matrix[2,1]:=0;    Matrix[2,2]:=1;
  HandleControl.MulMatrix(Matrix);
  Matrix[0,0]:=1;    Matrix[0,1]:=0;    Matrix[0,2]:=T.X;
  Matrix[1,0]:=0;    Matrix[1,1]:=1;    Matrix[1,2]:=T.Y;
  Matrix[2,0]:=0;    Matrix[2,1]:=0;    Matrix[2,2]:=1;
  HandleControl.MulMatrix(Matrix);
  HandleControl.Destroy;
end;

function TGroupControl.ShapeKind: TShapeKind;
begin
  result:=skGroup;
end;

function TGroupControl.Parameters: IShapeParameters;
begin
  Result:=FParameters;
end;

{ TEditPoint }

procedure TEditPoint.Change;
begin
if Assigned(FOnChange) then OnChange(Self);
end;

constructor TEditPoint.Create;
begin
 inherited;
 Self._AddRef;
end;

destructor TEditPoint.Destroy;
begin
 Self._Release;
 inherited;
end;

function TEditPoint.GetIndex: Integer;
begin
  Result:=FIndex;
end;

function TEditPoint.GetSelf: TEditPoint;
begin
  result:=Self;
end;

function TEditPoint.GetX: Integer;
begin
 Result:=FPoint.X;
end;

function TEditPoint.GetY: Integer;
begin
 Result:=FPoint.Y;
end;

function TEditPoint.Point: TPoint;
begin
  Result:=FPoint;
end;

procedure TEditPoint.SetIndex(value: Integer);
begin
  FIndex:=Value;
end;

procedure TEditPoint.SetX(value: Integer);
begin
  FPoint.X:=Value;
  Change;
end;

procedure TEditPoint.SetY(value: Integer);
begin
 FPoint.Y:=Value;
 Change;
end;

{ THandleControl }
procedure THandleControl.Binded(Shape: IShape);
var I:Integer;
 Rect:TRect;
 Quadrilateral:TQuadrilateral;
begin
  FShape:=Shape.GetSelf;
  if FShape Is TLineControl then
     begin
     For i:=0 to Length(FPOints)-1 do
       begin
       FPoints[i]:=nil;
       end;
     SetLength(FPoints,2);
     For i:=0 to Length(FPOints)-1 do
       begin
       FPoints[i]:=TEditPoint.Create;;
       FPoints[i].index:=i;
       FPoints[i].OnChange:=LineChange;
       end;
     FPoints[0].FPoint:=(FShape as TLineControl).Segment.P0;
     FPoints[1].FPoint:=(FShape as TLineControl).Segment.P1;
     end;
  if FShape Is TBezierControl then
     begin
     For i:=0 to Length(FPOints)-1 do
       begin
       FPoints[i]:=nil;
       end;
     SetLength(FPoints,4);
     For i:=0 to Length(FPOints)-1 do
       begin
       FPoints[i]:=TEditPoint.Create;
       FPoints[i].index:=i;
       FPoints[i].OnChange:=BezierChange;
       end;
     FPoints[0].FPoint:=(FShape as TBezierControl).Spline.P0;
     FPoints[1].FPoint:=(FShape as TBezierControl).Spline.P1;
     FPoints[2].FPoint:=(FShape as TBezierControl).Spline.P2;
     FPoints[3].FPoint:=(FShape as TBezierControl).Spline.P3;
     end;
  if FShape Is TImageControl then
     begin
     For i:=0 to Length(FPOints)-1 do
       begin
       FPoints[i]:=nil;
       end;
     SetLength(FPoints,5);
     For i:=0 to Length(FPOints)-1 do
       begin
       FPoints[i]:=TEditPoint.Create;
       FPoints[i].index:=i;
       FPoints[i].OnChange:=ImageChange;
       end;
     Quadrilateral:=(FShape as TImageControl).Image.Quadrilateral;
     FPoints[0].FPoint:=Quadrilateral.P0;
     FPoints[1].FPoint:=Quadrilateral.P1;
     FPoints[2].FPoint:=Quadrilateral.P2;
     FPoints[3].FPoint:=Quadrilateral.P3;
     end;
  if FShape Is TGroupControl then
     begin
     For i:=0 to Length(FPOints)-1 do
       begin
       FPoints[i]:=nil;
       end;
     SetLength(FPoints,5);
     For i:=0 to Length(FPOints)-1 do
       begin
       FPoints[i]:=TEditPoint.Create;
       FPoints[i].index:=i;
       FPoints[i].OnChange:=GroupChange;
       end;
     Rect:=(FShape as TGroupControl).AABBRect;
     Quadrilateral:=RectToQuadrilateral(Rect);
     FPoints[0].FPoint:=Quadrilateral.P0;
     FPoints[1].FPoint:=Quadrilateral.P1;
     FPoints[2].FPoint:=Quadrilateral.P2;
     FPoints[3].FPoint:=Quadrilateral.P3;
     end;
  if Assigned(FOnBinded) then OnBinded(Self);
end;

constructor THandleControl.Create;
begin
 //[!]
end;

destructor THandleControl.Destroy;
var i:Integer;
begin
  For i:=0 to Length(FPOints)-1 do
    begin
      FPoints[i]:=nil;
      end;
  SetLength(FPoints,0);

end;

procedure THandleControl.Draw;
begin
  if Assigned(FOnDraw) then FOnDraw(Self);
end;

function THandleControl.GetVisible: Boolean;
begin
  result:=FVisible;
end;

function THandleControl.HintPoint(Point: TPoint): Boolean;
var I:Integer;
 MinD, d:Real;
begin
  result:=False;
  MinD:=MaxInt;
  for i:=0 to Length(FPoints)-1 do
    begin
    d:=Distance(Point, FPoints[i].Point);
    if d< MinD then
      begin
      MinD:=d;
      FEditPoint:=FPoints[i];
      end;
    end;
  if (MinD>=0.0) and (MinD<HandleRadius) then
    Result:=True
end;

procedure THandleControl.LineChange(APoint: IEditPoint);
var LineControl:TLineControl;
begin
LineControl:=FShape as TLineControl;
 if APoint.Index=0 then
    LineControl.Segment.P0:=APoint.Point;
 if APoint.Index=1 then
    LineControl.Segment.P1:=APoint.Point;
end;

procedure THandleControl.BezierChange(APoint: IEditPoint);
var BezierControl:TBezierControl;
begin
BezierControl:=FShape as TBezierControl;
 if APoint.Index=0 then
    BezierControl.Spline.P0:=APoint.Point;
 if APoint.Index=1 then
    BezierControl.Spline.P1:=APoint.Point;
 if APoint.Index=2 then
    BezierControl.Spline.P2:=APoint.Point;
 if APoint.Index=3 then
    BezierControl.Spline.P3:=APoint.Point;
end;

procedure THandleControl.GroupChange(APoint: IEditPoint);
var GroupControl:TGroupControl;
Rect:TRect;
begin
// [!]
GroupControl:=FShape as TGroupControl;
Rect:=GroupControl.AABBRect;
 if APoint.Index=0 then
    begin
    Rect.TopLeft:=APoint.Point;
    end;
 if APoint.Index=1 then
    begin
    Rect.Right:=APoint.Point.X;
    Rect.Top:=APoint.Point.Y;
    end;
 if APoint.Index=2 then
    begin
    Rect.BottomRight:=APoint.Point;
    end;
 if APoint.Index=3 then
    begin
    Rect.Left:=APoint.Point.X;
    Rect.Bottom:=APoint.Point.Y;
    end;
GroupControl.Resize(Rect);

end;

procedure THandleControl.ImageChange(APoint: IEditPoint);
var ImageControl:TImageControl;
begin
// [!] ������ FreeTransform, ��������� ������ ���������������
ImageControl:=FShape as TImageControl;
 if APoint.Index=0 then
    begin
    ImageControl.Image.Quadrilateral.P0:=APoint.Point;
    ImageControl.Image.Quadrilateral.P3.X:=APoint.Point.X;
    ImageControl.Image.Quadrilateral.P1.Y:=APoint.Point.Y;
    end;
 if APoint.Index=1 then
    begin
    ImageControl.Image.Quadrilateral.P1:=APoint.Point;
    ImageControl.Image.Quadrilateral.P2.X:=APoint.Point.X;
    ImageControl.Image.Quadrilateral.P0.Y:=APoint.Point.Y;
    end;
 if APoint.Index=2 then
    begin
    ImageControl.Image.Quadrilateral.P2:=APoint.Point;
    ImageControl.Image.Quadrilateral.P1.X:=APoint.Point.X;
    ImageControl.Image.Quadrilateral.P3.Y:=APoint.Point.Y;
    end;
 if APoint.Index=3 then
    begin
    ImageControl.Image.Quadrilateral.P3:=APoint.Point;
    ImageControl.Image.Quadrilateral.P0.X:=APoint.Point.X;
    ImageControl.Image.Quadrilateral.P2.Y:=APoint.Point.Y;
    end;
// [!] �������� ��������.
 if APoint.Index=4 then
    begin
//     CenterPoint()
    end;
end;

function THandleControl.IntersectHandle(Point: TPoint): Boolean;
var I:Integer;
 MinD, d:Real;
begin
  result:=False;
  MinD:=MaxInt;
  for i:=0 to Length(FPoints)-1 do
    begin
    d:=Distance(Point, FPoints[i].Point);
    if d< MinD then
      begin
      MinD:=d;
      end;
    end;
  if (MinD>=0.0) and (MinD<HandleRadius) then
    Result:=True
end;

function THandleControl.SelectedPoint: IEditPoint;
begin
 result:=FEditPoint;
end;

procedure THandleControl.SetVisible(value: Boolean);
begin
  FVisible:=Value;
end;

procedure THandleControl.MulMatrix(M: TMatrix33);
var V:TVectorReal;
 i:Integer;
 HandleControl:THandleControl;
 GroupControl:TGroupControl;
begin
  if FShape Is TGroupControl then
    begin
    HandleControl:=THandleControl.Create;
    GroupControl:=FShape as TGroupControl;
    for i:=0 to Length(GroupControl.Group.Shapes)-1 do
       begin
       HandleControl.Binded(GroupControl.Group.Shapes[i]);
       HandleControl.MulMatrix(M);
       end;
    HandleControl.Destroy;
    end else
    begin
    for i:=0 to Length(FPoints)-1 do
       begin
       V:=VectorReal(FPoints[i].FPoint.X,FPoints[i].FPoint.Y);
       V:=MatrixMulVector(M,V);
       FPoints[i].X:=Round(V.X);
       FPoints[i].Y:=Round(V.Y);
       end;
    end;
end;

function THandleControl.ShapeKind: TShapeKind;
begin
  result:=skUndefine;
  if FShape Is TLineControl then
     Result:= skLine;
  if FShape Is TBezierControl then
     Result:= skBezier;
  if FShape Is TImageControl then
     Result:= skImage;
  if FShape Is TGroupControl then
     Result:= skGroup;
end;

function THandleControl.Shape: IShape;
begin
  case ShapeKind of
  skLine: result:= FShape as TLineControl;
  skBezier: result:= FShape as TBezierControl;
  skImage: result:= FShape as TImageControl;
  skGroup: result:= FShape as TGroupControl;
  end;
end;

{ TShapeProperty }

constructor TShapeParameters.Create;
begin
 Self._AddRef;
 FData:=TStringList.Create;
end;

procedure TShapeParameters.Delete(Index: Integer);
begin
 FData.Objects[Index]:=Nil;
 FData.Delete(Index);
end;

destructor TShapeParameters.Destroy;
begin
 Self._Release;
end;

function TShapeParameters.GetCount: Integer;
begin
 result:=FData.Count;
end;

function TShapeParameters.GetItem(index: Integer): TParameter;
begin
 result:=(FData.Objects[Index] as TParameter);
end;

function TShapeParameters.GetValue(name: String): Variant;
var Index:Integer;
begin
 result:=NULL;
 Index:=FData.IndexOf(Name);
 if Index>=0 then result:=(FData.Objects[Index] as TParameter).Value;
end;

procedure TShapeParameters.New(NickName: String;
  DataType: TParameterDataType; UnitStr: String; Protection: Boolean);
begin
 Count:=Count+1;
 Item[Count-1].NickName:=NickName;
 if DataType=pdtString then Item[Count-1].Value:='';
 if DataType=pdtNumeric then Item[Count-1].Value:=0;
 Item[Count-1].UnitStr:=UnitStr;
 Item[Count-1].Protection:=Protection;
end;

procedure TShapeParameters.SetCount(const Value: Integer);
begin
  while FData.Count>Value do
    begin
    FData.Objects[FData.Count-1]:=nil;
    FData.Delete(FData.Count-1);
    end;
  while FData.Count<Value do FData.AddObject('', TParameter.Create(FData));
end;

procedure TShapeParameters.SetItem(Index: Integer; const Value: TParameter);
begin
 FData.Objects[Index]:=Value;
end;

procedure TShapeParameters.SetValue(name: String; const Value: Variant);
var Index:Integer;
begin
 Index:=FData.IndexOf(Name);
 if Index>=0 then (FData.Objects[Index] as TParameter).Value:=Value;
end;

{ TParameter }

constructor TParameter.Create(AParent:TStringList);
begin
  Parent:=AParent;
  self._AddRef;
end;

function TParameter.DataType: TParameterDataType;
begin
 result:=pdtString;
 if VarIsStr(FValue) then result:=pdtString;
 if VarIsNumeric(FValue) then result:=pdtNumeric;
end;

destructor TParameter.Destroy;
begin
 self._Release;
 inherited;
end;

function TParameter.GetNickName: String;
begin
  result:=FNickName;
end;

function TParameter.GetProtection: Boolean;
begin
  Result:=FProtection;
end;

function TParameter.GetUnitStr: String;
begin
  result:=FUnitStr;
end;

function TParameter.GetValue: Variant;
begin
  Result:=FValue;
end;

procedure TParameter.SetNickName(const Value: String);
var Index:Integer;
begin
  FNickName:=Value;
  Index:=Parent.IndexOfObject(Self);
  Parent[Index]:=Value;
end;

procedure TParameter.SetProtection(const Value: Boolean);
begin
  FProtection:=Value;
end;

procedure TParameter.SetUnitStr(const Value: String);
begin
  FUnitStr:=Value;
end;

procedure TParameter.SetValue(const Value: Variant);
begin
 FValue:=Value;
end;



{ TPolyLineControl }

function TPolyLineControl.AABBRect: TRect;
begin
  Result:=Geometrs.AABBRect(PolyLine);
end;

constructor TPolyLineControl.Create;
begin
  OnDraw:=DefultePolyLineDraw;
  SetLength(PolyLine, 0);
  Pen:=TPen.Create;
  FParameters:=TShapeParameters.Create;
  FParameters.New('����������� �����������', pdtString, '', True);
  FParameters.New('����������� �����', pdtString, '', True);
  FParameters.New('������������', pdtString, '', True);
  Self._AddRef;
end;

constructor TPolyLineControl.Create(P0, P1: TPoint; APen: TPen);
begin
  OnDraw:=DefultePolyLineDraw;
  SetLength(PolyLine, 2);
  PolyLine[0]:=P0;
  PolyLine[1]:=P1;
  Pen:=TPen.Create;
  Pen.Assign(APen);
  FParameters:=TShapeParameters.Create;
  FParameters.New('����������� �����������', pdtString, '', True);
  FParameters.New('����������� �����', pdtString, '', True);
  FParameters.New('������������', pdtString, '', True);
  Self._AddRef;
end;


destructor TPolyLineControl.Destroy;
begin
  Pen.Free;
  Self._Release;
  inherited;
end;

function TPolyLineControl.Distance(Point: TPoint): Real;
begin
  Result:=Geometrs.Distance(Point, PolyLine);
end;

procedure TPolyLineControl.Draw;
begin
  if Assigned(FOnDraw) then FOnDraw(self);
end;

function TPolyLineControl.GetPixel(X, Y: Integer): T_RGBA;
begin
   if Distance(Point(X,Y))<Pen.Width then
    begin
    Result:=T_RGBA(Pen.Color);
    Result.A:=255;
    end else Result:=clTransperent;
end;

function TPolyLineControl.GetSelected: Boolean;
begin
  Result:=FSelected;
end;

function TPolyLineControl.GetSelf: TObject;
begin
  Result:=Self;
end;

function TPolyLineControl.InRect(Rect: TRect): Boolean;
begin
  Result:=PolyLineInRect(PolyLine, Rect)
end;

function TPolyLineControl.Parameters: IShapeParameters;
begin
  Result:=FParameters;
end;

procedure TPolyLineControl.SetSelected(value: Boolean);
begin
  FSelected:=Value;
end;

function TPolyLineControl.ShapeKind: TShapeKind;
begin
  result:=skPolyLine;
end;

procedure TPolyLineControl.ShiftTo(Offset: TPoint);
var i:Integer;
begin
  for i:=0 to Length(PolyLine)-1 do
    PolyLine[i]:=Add(PolyLine[i], Offset);
end;

end.
