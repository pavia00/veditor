unit UShapeSize;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids,
  Widgets;

type
  TFormShapeSize = class(TForm)
    StringGrid1: TStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure Update(HandleControl:THandleControl);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormShapeSize: TFormShapeSize;

implementation

uses Math;

{$R *.dfm}

procedure TFormShapeSize.FormCreate(Sender: TObject);
begin
  Top:=Screen.WorkAreaHeight-FormShapeSize.Height;
  Left:=Screen.WorkAreaWidth-FormShapeSize.Width;

  StringGrid1.Cells[0,0]:='X';
  StringGrid1.Cells[0,1]:='Y';
  StringGrid1.Cells[0,2]:='������';
  StringGrid1.Cells[0,3]:='������';
  StringGrid1.Cells[0,4]:='����';
end;

procedure TFormShapeSize.Update(HandleControl: THandleControl);
var
 Rect:TRect;
 Angle:Real;
 C:TPoint;
 LineControl:TLineControl;
begin
if  HandleControl.Shape<>nil then
  begin
  Rect:=HandleControl.Shape.AABBRect;
  StringGrid1.Cells[1,0]:=IntToStr(Rect.Left);
  StringGrid1.Cells[1,1]:=IntToStr(Rect.Top);
  StringGrid1.Cells[1,2]:=IntToStr(Rect.Right-Rect.Left);
  StringGrid1.Cells[1,3]:=IntToStr(Rect.Bottom-Rect.Top);
  StringGrid1.Cells[1,4]:='0';
  if HandleControl.Shape.GetSelf is TLineControl then
     begin
     LineControl:=HandleControl.Shape.GetSelf as TLineControl;
     C.X:=(LineControl.Segment.P0.X+LineControl.Segment.P1.X) div 2;
     C.Y:=(LineControl.Segment.P0.Y+LineControl.Segment.P1.Y) div 2;
     Angle:=ArcTan2(LineControl.Segment.P1.Y-C.Y, LineControl.Segment.P1.X-C.X);
     if Angle<0 then Angle:=Angle +2*Pi;
     Angle:=(Angle+Pi/2);
     if Angle>2*Pi then Angle:=Angle -2*Pi;
     StringGrid1.Cells[1,4]:=Format('%.2f',[Angle/Pi*180]);
     end;
  end;
end;

end.
