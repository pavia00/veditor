object DefShapeProperty: TDefShapeProperty
  Left = 192
  Top = 124
  Width = 304
  Height = 345
  Caption = #1054#1087#1088#1077#1076#1077#1083#1080#1090#1100' '#1089#1074#1086#1081#1089#1090#1074#1072' '#1092#1080#1075#1091#1088
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 0
    Top = 129
    Width = 288
    Height = 118
    Align = alClient
    ColCount = 3
    FixedColor = clGradientInactiveCaption
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
    TabOrder = 0
  end
  object Footer: TPanel
    Left = 0
    Top = 247
    Width = 288
    Height = 59
    Align = alBottom
    TabOrder = 1
    object Button1: TButton
      Left = 24
      Top = 16
      Width = 75
      Height = 25
      Caption = #1057#1086#1079#1076#1072#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 104
      Top = 16
      Width = 75
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object Header: TPanel
    Left = 0
    Top = 0
    Width = 288
    Height = 129
    Align = alTop
    TabOrder = 2
    object Label2: TLabel
      Left = 24
      Top = 64
      Width = 19
      Height = 13
      Caption = #1056#1086#1076
    end
    object Label1: TLabel
      Left = 24
      Top = 24
      Width = 52
      Height = 13
      Caption = #1055#1086#1079#1099#1074#1085#1086#1081
    end
    object Label3: TLabel
      Left = 24
      Top = 96
      Width = 39
      Height = 13
      Caption = #1047#1072#1097#1080#1090#1072
    end
    object Edit1: TEdit
      Left = 88
      Top = 24
      Width = 121
      Height = 21
      Enabled = False
      TabOrder = 0
      Text = #1055#1072#1088#1072#1084#1077#1090#1088'1'
    end
    object ComboBox1: TComboBox
      Left = 88
      Top = 64
      Width = 121
      Height = 21
      Enabled = False
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 1
      Text = #1095#1080#1089#1083#1080#1090#1077#1083#1100#1085#1099#1081
      Items.Strings = (
        #1095#1080#1089#1083#1080#1090#1077#1083#1100#1085#1099#1081
        #1089#1090#1088#1086#1082#1086#1074#1099#1081)
    end
    object ComboBox2: TComboBox
      Left = 88
      Top = 96
      Width = 121
      Height = 21
      Enabled = False
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 2
      Text = #1073#1077#1079#1079#1072#1097#1080#1090#1085#1086
      Items.Strings = (
        #1073#1077#1079#1079#1072#1097#1080#1090#1085#1086
        #1079#1072#1097#1080#1097#1077#1085#1086)
    end
  end
end
