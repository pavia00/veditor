unit Geometrs;

interface

uses Types, Math;

const
  NilRect:TRect=(Left:0; Top:0; Right:00; Bottom:0;);
  MaxReal=MaxDouble;
type
  TScalar=Integer;
  TSegment=packed record
    P0,P1:TPoint;
    end;
  TVector=TPoint;
  TVectorReal=packed record
    X,Y:Real;
    end;

  TMatrix33=array [0..2] of array [0..2] of Real;

  TQuadrilateral=packed record   // ������ ��������
    P0, P1, P2, P3:TPoint;
    end;

  TPolynom2=packed record
    a0, a1: Real;
    end;
  TPolynom3=packed record
    a0, a1, a2:Real;
    end;

  TPolynom4=packed record
    a0, a1, a2, a3:Real;
    end;

  TPolynom4Point=packed record        // ������� ��� ��������
    a0, a1, a2, a3:TPoint;
    end;

  TSpline=packed record              // ����������� ����������� �� ��������� ������ �����.
    P0, P1, P2, P3:TPoint;    // 4 - ����� ����� ������� �������� ����� � ����� 1/3 �� t.
    end;

  TBezier=packed record
    B0, H1, H2, B3: TPoint;
    end;

  TPolyLine=array of TPoint;

  TArrayVectorReal=array of TVectorReal;

function Segment(P0, P1:TPoint):TSegment;
function Spline(P0, P1, P2, P3:TPoint):TSpline;
function Add(const A, B:TVector):TVector;
function Sub(const A, B:TVector):TVector;
function Mul(const A:Real; B:TVector):TVector;  overload;
function Mul(const A, B:TVector):TVector; overload;
function DotProd(const A,B:TVector):Real;
function Norm(const A:TVector):Real;
function Distance(const A, B:TVector):Real; overload;
function Distance(const P:TPoint; const Segment:TSegment):Real; overload;
function Distance(const P:TPoint; const PolyLine:TPolyLine):Real; overload;
function Distance(const P:TPoint; const Bezier:TSpline):Real; overload;
function PointInRect(Point:TPoint; Rect:TRect):Boolean;
function PointInQuadrilateral(Point:TPoint; Quadrilateral:TQuadrilateral):Boolean;
function SegmentInRect(Segment:TSegment; Rect:TRect):Boolean;
function RectInRect(AABBRect:TRect; OutRect:TRect):Boolean;
function QuadrilateralInRect(Quadrilateral:TQuadrilateral; OutRect:TRect):Boolean;
function AABBRect(Segment:TSegment):TRect;  overload;
function BezierToPolynom(Bezier:TBezier):TPolynom4Point;
function PolynomAtT(var Polynom:TPolynom4Point; var t:TVectorReal):TVector;
function Max(const A,B:TVector):TVector; overload;
function Min(const A,B:TVector):TVector; overload;
function Max(const A,B:TVectorReal):TVectorReal; overload;
function Min(const A,B:TVectorReal):TVectorReal; overload;
function VectorReal(const X,Y:Real):TVectorReal;
function Polynom(a0, a1:Real):TPolynom2; overload;
function Polynom(a0, a1, a2:Real):TPolynom3; overload;
function EvalRoots(Polynom:TPolynom2; var Goal:Real):Integer; overload;
function EvalRoots(Polynom:TPolynom3; var Goal:TPolynom2):Integer; overload;
//function EvalRoots(a:TPolynomN; var Goal:TPolynomN):Integer; overload;
function AABBRect(Bezier:TBezier):TRect;  overload;
function SplineToBezier(Spline:TSpline):TBezier;
function BezierToSpline(Bezier:TBezier):TSpline;
function BezierInRect(Bezier:TBezier; OutRect:TRect):Boolean;
function PolyLineInRect(PolyLine:TPolyLine; OutRect:TRect):Boolean;
function SplineInRect(Spline:TSpline; OutRect:TRect):Boolean;
function RectToQuadrilateral(Rect:TRect):TQuadrilateral;
function Distance(const Point:TPoint; const Rect:TRect):Real; overload;
function Distance(const Point:TPoint; const Quadrilateral:TQuadrilateral):Real; overload;
function AABBRect(Quadrilateral:TQuadrilateral):TRect;  overload;
function AABBRect(PolyLine:TPolyLine):TRect;  overload;
function AddBound(Rect:TRect; B:Integer):TRect;
function MatrixMulVector(M:TMatrix33; V:TVectorReal):TVectorReal;

implementation

function Segment(P0, P1:TPoint):TSegment;
begin
Result.P0:=P0;
Result.P1:=P1;
end;

function Spline(P0, P1, P2, P3:TPoint):TSpline;
begin
Result.P0:=P0;
Result.P1:=P1;
Result.P2:=P2;
Result.P3:=P3;
end;

function Add(const A, B:TVector):TVector;
begin
Result.X:=A.X+B.X;
Result.Y:=A.Y+B.Y;
end;

function Sub(const A, B:TVector):TVector;
begin
Result.X:=A.X-B.X;
Result.Y:=A.Y-B.Y;
end;

function Mul(const A:Real; B:TVector):TVector;  overload;
begin
Result.X:=Round(A*B.X);
Result.Y:=Round(A*B.Y);
end;

function Mul(const A, B:TVector):TVector; overload;
begin
Result.X:=A.X*B.X;
Result.Y:=A.Y*B.Y;
end;

function DotProd(const A,B:TVector):Real;
begin
Result:=A.X*B.X+A.Y*B.Y;
end;

function Norm(const A:TVector):Real;
begin
Result:=Math.Hypot(A.X, A.Y);
end;

function Distance(const A, B:TVector):Real; overload;
begin
result:=Norm(Sub(A,B))
end;

// http://geomalgorithms.com/a02-_lines.html#Distance-to-Ray-or-Segment
function Distance(const P:TPoint; const Segment:TSegment):Real; overload;
var
 BA:TVector;
 PA:TVector;
 c, cos_BA_BA, cos_BA_PA:Real;
 Ph:TPoint;

begin
  BA := Sub(Segment.P1, Segment.P0);
  PA := Sub(P, Segment.P0);

  cos_BA_PA := DotProd(PA,BA);
  if ( cos_BA_PA <= 0.0) then
     result:=  Distance(P, Segment.P0)
     else
     begin
       cos_BA_BA := DotProd(BA,BA);
       if ( cos_BA_BA <= cos_BA_PA ) then
          result:=  Distance(P, Segment.P1)
          else
          begin
          c := cos_BA_PA / cos_BA_BA;
          Ph.X := Round(Segment.P0.X + c * BA.X);
          Ph.Y := Round(Segment.P0.Y + c * BA.Y);
          result:= Distance(P, Ph);
          end;
     end;
end;

function Distance(const P:TPoint; const PolyLine:TPolyLine):Real; overload;
var
  P1,P2:TPoint;
  i:Integer;
begin
  if Length(PolyLine)=0 then
     Result:=MaxReal
     else
     if Length(PolyLine)=1 then
        begin
        P1:=PolyLine[0];
        result:=Distance(P, P1);
        end else
        begin
        P1:=PolyLine[0];
        P2:=PolyLine[1];
        result:=Distance(P, Segment(P1,P2));
        for i:=0 to Length(PolyLine)-2 do
          begin
          P1:=PolyLine[i];
          P2:=PolyLine[i+1];
          result:=Min(result, Distance(P, Segment(P1,P2)));
          end;
        end;

end;

function Distance(const P:TPoint; const Bezier:TSpline):Real; overload;
begin
  result:=Min(Distance(P, Segment(Bezier.P0,Bezier.P1)),
              Distance(P, Segment(Bezier.P1,Bezier.P2)));
  result:=Min(result,
              Distance(P, Segment(Bezier.P2,Bezier.P3)));
end;

function PointInRect(Point:TPoint; Rect:TRect):Boolean;
begin
 if  (Rect.Left<=Point.X) and (Point.X<Rect.Right) and
     (Rect.Top<=Point.Y) and (Point.Y<Rect.Bottom) then
       Result:=True
     else
       Result:=False;
end;

function PointInQuadrilateral(Point:TPoint; Quadrilateral:TQuadrilateral):Boolean;
  // �������� ����������� ���� � ���� X � �������.
  function SegmentCheck(Point:TPoint; Segment:TSegment):Boolean;
  var dX, dY:Integer;
   X:Real;
  begin
  Result:=False;
  if (Segment.P0.Y<=Point.Y) and (Point.Y<=Segment.P1.Y) or
     (Segment.P1.Y<=Point.Y) and (Point.Y<=Segment.P0.Y) then
     begin
     dX:=Segment.P1.X-Segment.P0.X;
     dY:=Segment.P1.Y-Segment.P0.Y;
     if dY=0 then  Result:=True
        else
        begin
        X:=Segment.P0.X+dX/dY*(Point.Y-Segment.P0.Y);
        if Point.X<=X then Result:=True;
        end;
     end;
  end;
var
  Segment01:TSegment;
  Segment12:TSegment;
  Segment23:TSegment;
  Segment30:TSegment;
  sum:Integer;
begin
   Segment01:=Segment(Quadrilateral.P0, Quadrilateral.P1);
   Segment12:=Segment(Quadrilateral.P1, Quadrilateral.P2);
   Segment23:=Segment(Quadrilateral.P2, Quadrilateral.P3);
   Segment30:=Segment(Quadrilateral.P3, Quadrilateral.P0);
   sum:=ord(SegmentCheck(Point, Segment01))+ord(SegmentCheck(Point, Segment12))+
        ord(SegmentCheck(Point, Segment23))+ord(SegmentCheck(Point, Segment30));
   result:=False;
   if Sum and 1=1 then result:=True;
end;

function SegmentInRect(Segment:TSegment; Rect:TRect):Boolean;
begin
 if PointInRect(Segment.P0, Rect) and
    PointInRect(Segment.P1, Rect) then
      Result:=True
    else
      Result:=False;
end;

function RectInRect(AABBRect:TRect; OutRect:TRect):Boolean;
begin
result:=PointInRect(AABBRect.TopLeft, OutRect) and
        PointInRect(AABBRect.BottomRight, OutRect);
end;

function QuadrilateralInRect(Quadrilateral:TQuadrilateral; OutRect:TRect):Boolean;
begin
result:=PointInRect(Quadrilateral.P0, OutRect) and
        PointInRect(Quadrilateral.P1, OutRect) and
        PointInRect(Quadrilateral.P2, OutRect) and
        PointInRect(Quadrilateral.P3, OutRect);
end;

function AABBRect(Segment:TSegment):TRect;  overload;
begin
if (Segment.P0.X<Segment.P1.X) then
   begin
   Result.Left:=Segment.P0.X;
   Result.Right:=Segment.P1.X;
   end else
   begin
   Result.Left:=Segment.P1.X;
   Result.Right:=Segment.P0.X;
   end;

if (Segment.P0.Y<Segment.P1.Y) then
   begin
   Result.Top:=Segment.P0.Y;
   Result.Bottom:=Segment.P1.Y;
   end else
   begin
   Result.Top:=Segment.P1.Y;
   Result.Bottom:=Segment.P0.Y;
   end;
end;

function BezierToPolynom(Bezier:TBezier):TPolynom4Point;
begin
  Result.a0:=Bezier.B0;
  Result.a1.X:=-3*Bezier.B0.X+3*Bezier.H1.X;
  Result.a1.Y:=-3*Bezier.B0.Y+3*Bezier.H1.Y;
  Result.a2.X:=3*Bezier.B0.X-6*Bezier.H1.X+3*Bezier.H2.X;
  Result.a2.Y:=3*Bezier.B0.Y-6*Bezier.H1.Y+3*Bezier.H2.Y;
  Result.a3.X:=-Bezier.B0.X+3*Bezier.H1.X-3*Bezier.H2.X+Bezier.B3.X;
  Result.a3.Y:=-Bezier.B0.Y+3*Bezier.H1.Y-3*Bezier.H2.Y+Bezier.B3.Y;
end;

function PolynomAtT(var Polynom:TPolynom4Point; var t:TVectorReal):TVector;
begin
Result.X:=Round(Polynom.a0.X+Polynom.a1.X*t.X+Polynom.a2.X*t.X*t.X+Polynom.a3.X*t.X*t.X*t.X);
Result.Y:=Round(Polynom.a0.Y+Polynom.a1.Y*t.Y+Polynom.a2.Y*t.Y*t.Y+Polynom.a3.Y*t.Y*t.Y*t.Y);
end;

function Max(const A,B:TVector):TVector; overload;
begin
Result.X:=Max(A.X, B.X);
Result.Y:=Max(A.Y, B.Y);
end;

function Min(const A,B:TVector):TVector; overload;
begin
Result.X:=Min(A.X, B.X);
Result.Y:=Min(A.Y, B.Y);
end;

function Max(const A,B:TVectorReal):TVectorReal; overload;
begin
Result.X:=Max(A.X, B.X);
Result.Y:=Max(A.Y, B.Y);
end;

function Min(const A,B:TVectorReal):TVectorReal; overload;
begin
Result.X:=Min(A.X, B.X);
Result.Y:=Min(A.Y, B.Y);
end;

function VectorReal(const X,Y:Real):TVectorReal;
begin
   Result.X:=X;
   Result.Y:=Y;
end;

function Polynom(a0, a1:Real):TPolynom2; overload;
begin
  result.a0:=a0;
  result.a1:=a1;
end;

function Polynom(a0, a1, a2:Real):TPolynom3; overload;
begin
  result.a0:=a0;
  result.a1:=a1;
  result.a2:=a2;
end;

function EvalRoots(Polynom:TPolynom2; var Goal:Real):Integer; overload;
begin
 if Polynom.a1=0 then
   begin
    Result:=0;
    Goal:=0;
   end else
    begin
      Result:=1;
      Goal:=-Polynom.a0/Polynom.a1;
    end;
end;

function EvalRoots(Polynom:TPolynom3; var Goal:TPolynom2):Integer; overload;
var SubPolynom:TPolynom2;
var a,b,c,D:Real;
begin
 Goal.a0:=0;
 Goal.a1:=0;
 if Polynom.a2=0 then   // ��������� ������ ������ ��� ��� � ����������� ��� ��������
   begin
   SubPolynom:=Geometrs.Polynom(Polynom.a0, Polynom.a1);
   Result:=EvalRoots(SubPolynom, Goal.a0);
   end  else
    begin
      a:=Polynom.a2;
      b:=Polynom.a1;
      c:=Polynom.a0;
      D:=b*b-4*a*c;
      if d<0 then  Result:=0;
      if d>0 then
         begin
           Result:=2;
           Goal.a0:=(-b+sqrt(D))/(2*a);
           Goal.a1:=(-b-sqrt(D))/(2*a);
         end;
      if d=0 then // [!]�������� �� ��������� � �������� ���� ����� eq(d/b,0, EpsReal)
         begin
           Result:=1;
           Goal.a0:=-b/(2*a);
         end;
    end;
end;

// ���������� 
{
function EvalRoots(a:TPolynomN; var Goal:TPolynomN):Integer; overload;
var
  companion:TMatrixNN;
begin
 n:=Length(a);
 if (n=0) then
    begin
    end;
 if (n=1) then
    begin
    end;
 if n>1 then
    begin
    SetSize(companion, N-1, N-1);
    for i:=0 to N-3 do
      companion[i,i-1]:=1;
    for i:=0 to N-2 do
      companion[0,i]:=a[i+1]/a[0];
    Goal:=EnginValue(a);
    end;
end;
}

function AABBRect(Bezier:TBezier):TRect;  overload;
var
  Polynom:TPolynom4Point;
  a,b,c:TVector;
  t1,t2:TVectorReal;
  TZero, TOne:TVectorReal;
  P0,P1,P2,P3:TVector;
  Roots:TPolynom2;
begin
  Result:=NilRect;
  Polynom:=BezierToPolynom(Bezier);
  a:=Mul(3,Polynom.a3);
  b:=Mul(2,Polynom.a2);
  c:=Mul(1,Polynom.a1);
  EvalRoots(Geometrs.Polynom(c.X, b.X, a.X), Roots);
  t1.X:=Roots.a0;  t2.X:=Roots.a1;
  EvalRoots(Geometrs.Polynom(c.Y, b.Y, a.Y), Roots);
  t1.Y:=Roots.a0;  t2.Y:=Roots.a1;
   begin   // �������� �������� � ��� ������ ����� ��� ��� ����� ������ ��� TZero � ��� ������ ������ �������� ��� "�������������"
      TZero:=VectorReal(0,0); TOne:=VectorReal(1,1);
      t1:=Min(Max(TZero,t1),TOne);
      t2:=Min(Max(TZero,t2),TOne);
      P0:=PolynomAtT(Polynom, TZero);
      P1:=PolynomAtT(Polynom, t1);
      P2:=PolynomAtT(Polynom, t2);
      P3:=PolynomAtT(Polynom, TOne);
      Result.TopLeft:=Min(Min(P0,P1), Min(P2, P3));
      Result.BottomRight:=Max(Max(P0,P1), Max(P2, P3));
    end;
end;

// ��������� � �����
function SplineToBezier(Spline:TSpline):TBezier;
begin
Result.B0:=Spline.P0;
Result.B3:=Spline.P3;
Result.H1.X:=Round(Spline.P3.X/3-3*Spline.P2.X/2+3*Spline.P1.X-5*Spline.P0.X/6);
Result.H1.Y:=Round(Spline.P3.Y/3-3*Spline.P2.Y/2+3*Spline.P1.Y-5*Spline.P0.Y/6);
Result.H2.X:=Round(-5*Spline.P3.X/6+3*Spline.P2.X-3*Spline.P1.X/2+Spline.P0.X/3);
Result.H2.Y:=Round(-5*Spline.P3.Y/6+3*Spline.P2.Y-3*Spline.P1.Y/2+Spline.P0.Y/3);
end;

function BezierToSpline(Bezier:TBezier):TSpline;
begin
Result.P0:=Bezier.B0;
Result.P3:=Bezier.B3;
Result.P1.X:=Round(8/27*Bezier.B0.X+8/9*Bezier.H1.X+4/9*Bezier.H2.X+1/27*Bezier.B3.X);
Result.P1.Y:=Round(8/27*Bezier.B0.Y+8/9*Bezier.H1.Y+4/9*Bezier.H2.Y+1/27*Bezier.B3.Y);
Result.P2.X:=Round(1/27*Bezier.B0.X+4/9*Bezier.H1.X+8/9*Bezier.H2.X+8/27*Bezier.B3.X);
Result.P2.Y:=Round(1/27*Bezier.B0.Y+4/9*Bezier.H1.Y+8/9*Bezier.H2.Y+8/27*Bezier.B3.Y);
end;

function BezierInRect(Bezier:TBezier; OutRect:TRect):Boolean;
begin
  result:=RectInRect(AABBRect(Bezier), OutRect);
end;

function SplineInRect(Spline:TSpline; OutRect:TRect):Boolean;
begin
    result:=BezierInRect(SplineToBezier(Spline), OutRect);
end;

function PolyLineInRect(PolyLine:TPolyLine; OutRect:TRect):Boolean;
begin
  result:=RectInRect(AABBRect(PolyLine), OutRect);
end;

function RectToQuadrilateral(Rect:TRect):TQuadrilateral;
begin
Result.P0:=Rect.TopLeft;
Result.P2:=Rect.BottomRight;
Result.P1.X:=Result.P2.X;
Result.P1.Y:=Result.P0.Y;
Result.P3.X:=Result.P0.X;
Result.P3.Y:=Result.P2.Y;
end;

function Distance(const Point:TPoint; const Rect:TRect):Real; overload;
var
  Quadrilateral:TQuadrilateral;
  Segment01:TSegment;
  Segment12:TSegment;
  Segment23:TSegment;
  Segment30:TSegment;
  d:Real;
begin
if PointInRect(Point, Rect) then Result:=0
   else
   begin
   RectToQuadrilateral(Rect);
   Segment01:=Segment(Quadrilateral.P0, Quadrilateral.P1);
   Segment12:=Segment(Quadrilateral.P1, Quadrilateral.P2);
   Segment23:=Segment(Quadrilateral.P2, Quadrilateral.P3);
   Segment30:=Segment(Quadrilateral.P3, Quadrilateral.P0);
   Result:=Distance(Point, Segment01);
   d:=Distance(Point, Segment12);
   if  d<Result then result:=d;
   d:=Distance(Point, Segment23);
   if  d<Result then result:=d;
   d:=Distance(Point, Segment30);
   if  d<Result then result:=d;
   end;
end;

function Distance(const Point:TPoint; const Quadrilateral:TQuadrilateral):Real; overload;
var
  Segment01:TSegment;
  Segment12:TSegment;
  Segment23:TSegment;
  Segment30:TSegment;
  d:Real;
begin
if PointInQuadrilateral(Point, Quadrilateral) then Result:=0
   else
   begin
   Segment01:=Segment(Quadrilateral.P0, Quadrilateral.P1);
   Segment12:=Segment(Quadrilateral.P1, Quadrilateral.P2);
   Segment23:=Segment(Quadrilateral.P2, Quadrilateral.P3);
   Segment30:=Segment(Quadrilateral.P3, Quadrilateral.P0);
   Result:=Distance(Point, Segment01);
   d:=Distance(Point, Segment12);
   if  d<Result then result:=d;
   d:=Distance(Point, Segment23);
   if  d<Result then result:=d;
   d:=Distance(Point, Segment30);
   if  d<Result then result:=d;
   end;
end;

function AABBRect(Quadrilateral:TQuadrilateral):TRect;  overload;
begin
 Result.Left:=Min(Min(Quadrilateral.P0.X,Quadrilateral.P1.X),
                  Min(Quadrilateral.P2.X,Quadrilateral.P3.X));
 Result.Right:=Max(Max(Quadrilateral.P0.X,Quadrilateral.P1.X),
                   Max(Quadrilateral.P2.X,Quadrilateral.P3.X));
 Result.Top:=Min(Min(Quadrilateral.P0.Y,Quadrilateral.P1.Y),
                 Min(Quadrilateral.P2.Y,Quadrilateral.P3.Y));
 Result.Bottom:=Max(Max(Quadrilateral.P0.Y,Quadrilateral.P1.Y),
                    Max(Quadrilateral.P2.Y,Quadrilateral.P3.Y));

end;

function AABBRect(PolyLine:TPolyLine):TRect;  overload;
var MinX,MaxX,MinY,MaxY:Integer;
 i:Integer;
begin
if Length(PolyLine)=0 then
   Result:=NilRect;
if Length(PolyLine)>=1 then
   begin
   MinX:=PolyLine[0].X;
   MaxX:=PolyLine[0].X;

   MinY:=PolyLine[0].Y;
   MaxY:=PolyLine[0].Y;
   for i:=1 to Length(PolyLine)-1 do
     begin
     MinX:=Min(MinX,PolyLine[i].X);
     MaxX:=Max(MaxX,PolyLine[i].X);

     MinY:=Min(MinY,PolyLine[i].Y);
     MaxY:=Max(MaxY,PolyLine[i].Y);
     end;
   result:=AABBRect(Segment(Point(MinX,MinY),Point(MaxX,MaxY)));
   end;


end;

function AddBound(Rect:TRect; B:Integer):TRect;
begin
Result.Left:=Rect.Left-B;
Result.Right:=Rect.Right+B;
Result.Top:=Rect.Top-B;
Result.Bottom:=Rect.Bottom+B;

end;

function MatrixMulVector(M:TMatrix33; V:TVectorReal):TVectorReal;
begin
result.X:=M[0,0]*V.X+M[0,1]*V.Y+M[0,2];
result.Y:=M[1,0]*V.Y+M[1,1]*V.Y+M[1,2];
end;

end.
