unit UImageSave;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,
  jpeg, pngimage, GIFImage, LibTiffDelphi, ColorGrd;

type
  TFormSaveImage = class(TForm)
    pnlConfigs: TPanel;
    pnlPreview: TPanel;
    TransperentPanel: TGroupBox;
    FormatPanel: TRadioGroup;
    btnOk: TButton;
    btnCancel: TButton;
    SavePanel: TPanel;
    PaintBox1: TPaintBox;
    CompressPanel: TGroupBox;
    lblCompress: TLabel;
    sbarCompress: TScrollBar;
    TransperentCheck: TCheckBox;
    ColorBox1: TColorBox;
    procedure FormShow(Sender: TObject);
    procedure FormatPanelClick(Sender: TObject);
    procedure sbarCompressChange(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure pnlPreviewCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure PaintBox1Paint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ColorBox1Change(Sender: TObject);
  private
    { Private declarations }
    FBitmap:TBitmap;
    FPng:TPNGObject;
    procedure SavePicture;
    procedure ChangeFormat;
  public
    FileName:String;
    { Public declarations }
  end;

var
  FormSaveImage: TFormSaveImage;

procedure PngToBitmap(Png:TPNGObject; var bmp:TBitmap; NegAlfa:Boolean);
procedure BitmapToPng(bmp:TBitmap; var Png:TPNGObject; NegAlfa:Boolean);
function  LoadImage(FileName:String):TBitmap;
procedure SavePicture(FileName:String; Bitmap:TBitmap; CompressionValue:Integer);

implementation

uses Unit1;

{$R *.dfm}

// http://www.asmail.be/msg0055571626.html
procedure TIFFReadRGBAImageSwapRB(Width,Height: Cardinal; Memory: Pointer);
{$IFDEF DELPHI_5}
type
  PCardinal = ^Cardinal;
{$ENDIF}
var
  m: PCardinal;
  n: Cardinal;
  o: Cardinal;
begin
  m:=Memory;
  for n:=0 to Width*Height-1 do
  begin
    o:=m^;
    m^:= (o and $FF00FF00) or                {G and A}
        ((o and $00FF0000) shr 16) or        {B}
        ((o and $000000FF) shl 16);          {R}
    Inc(m);
  end;
end;

procedure PngToBitmap(Png:TPNGObject; var bmp:TBitmap; NegAlfa:Boolean);
type
  T_RGB=packed record
    B,G,R:Byte;
    end;
  T_RGBA=packed record
    B, G, R, A:Byte;
    end;

  TA_Byte=array [0..65536] of Byte;
  PA_Byte=^TA_Byte;
  TA_RGB=array [0..65536] of T_RGB;
  PA_RGB=^TA_RGB;
  TA_RGBA=array [0..65536] of T_RGBA;
  PA_RGBA=^TA_RGBA;
var X,Y:Integer;
   BmpLine:PA_RGBA;
   PngLine:PA_RGB;
   PngLineAlpha:PA_Byte;
begin
if png.Transparent then
  begin
  bmp.PixelFormat:=pf32bit;
  bmp.Width:=Png.Width;
  bmp.Height:=Png.Height;
  for Y:=0 to bmp.Height-1 do
    begin
    Pointer(BmpLine):=bmp.ScanLine[Y];
    Pointer(PngLine):=png.Scanline[Y];
    Pointer(PngLineAlpha):=png.AlphaScanline[Y];
    for X:=0 to bmp.Width-1 do
        begin
          BmpLine[X].B:=PngLine[X].B;
          BmpLine[X].G:=PngLine[X].G;
          BmpLine[X].R:=PngLine[X].R;
          if NegAlfa then
             BmpLine[X].A:=255- PngLineAlpha[X]
             else
             BmpLine[X].A:=PngLineAlpha[X]
        end;
    end;
  end else
  begin
  Png.Assign(Bmp);
  end;
end;

procedure BitmapToPng(bmp:TBitmap; var Png:TPNGObject; NegAlfa:Boolean);
type
  T_RGB=packed record
    B,G,R:Byte;
    end;
  T_RGBA=packed record
    B, G, R, A:Byte;
    end;

  TA_Byte=array [0..65536] of Byte;
  PA_Byte=^TA_Byte;
  TA_RGB=array [0..65536] of T_RGB;
  PA_RGB=^TA_RGB;
  TA_RGBA=array [0..65536] of T_RGBA;
  PA_RGBA=^TA_RGBA;

var X,Y:Integer;
   BmpLine:PA_RGBA;
   PngLine:PA_RGB;
   PngLineAlpha:PA_Byte;
begin
  png.Assign(Bmp);
  if Bmp.PixelFormat=pf32bit then
     begin
     Png.CreateAlpha;

     for Y:=0 to bmp.Height-1 do
       begin
       Pointer(BmpLine):=bmp.ScanLine[Y];
       Pointer(PngLine):=png.Scanline[Y];
       Pointer(PngLineAlpha):=png.AlphaScanline[Y];
       if NegAlfa then
          begin
          for X:=0 to bmp.Width-1 do
           PngLineAlpha[X]:=255- BmpLine[X].A;
          end else
          begin
          for X:=0 to bmp.Width-1 do
           PngLineAlpha[X]:=BmpLine[X].A;
          end;
       end;
     end;
end;

procedure SetTransparentColor(Bitmap:TBitmap; var Png:TPNGObject; Color:TColor);
type
  T_Color=packed record
    R,G,B,A:Byte;
    end;
  T_RGB=packed record
    B,G,R:Byte;
    end;
  T_RGBA=packed record
    B, G, R, A:Byte;
    end;

  TA_Byte=array [0..65536] of Byte;
  PA_Byte=^TA_Byte;
  TA_RGB=array [0..65536] of T_RGB;
  PA_RGB=^TA_RGB;
  TA_RGBA=array [0..65536] of T_RGBA;
  PA_RGBA=^TA_RGBA;

var X,Y:Integer;
   BmpLine:PA_RGBA;
   PngLine:PA_RGB;
   PngLineAlpha:PA_Byte;
begin
  png.Assign(Bitmap);
  Png.CreateAlpha;  
     for Y:=0 to Bitmap.Height-1 do
       begin
       Pointer(BmpLine):=Bitmap.ScanLine[Y];
       Pointer(BmpLine):=Bitmap.ScanLine[Y];
       Pointer(PngLine):=png.Scanline[Y];
       Pointer(PngLineAlpha):=png.AlphaScanline[Y];
       for X:=0 to Bitmap.Width-1 do
         begin
           if (BmpLine[X].R=T_Color(Color).R) and
              (BmpLine[X].G=T_Color(Color).G) and
              (BmpLine[X].B=T_Color(Color).B) then
                 PngLineAlpha[X]:=0
                 else
                 PngLineAlpha[X]:=255;
         end;
       end;

end;

function LoadImage(FileName:String):TBitmap;
var
  Ext:String;
  jpg: TJPEGImage;
  PNG: TPNGObject;
  GiF: TGIFImage;
var
  OpenTiff: PTIFF;
  FirstPageWidth,FirstPageHeight: Cardinal;
  FirstPageBitmap: TBitmap;

begin
Ext:=UpperCase(ExtractFileExt(FileName));
if (Ext='.BMP') then
 begin
 Result:=TBitmap.Create;
 Result.LoadFromFile(FileName);
 end;
if (Ext='.JPEG') or (Ext='.JPG') then
 begin
 Result:=TBitmap.Create;
 jpg:=TJPEGImage.Create;
 jpg.LoadFromFile(FileName);
 Result.Assign(jpg);
 jpg.Free;
 end;
if (Ext='.PNG') then
 begin
 Result:=TBitmap.Create;
 Result.PixelFormat:=pf32bit;
 PNG := TPNGObject.Create;
 PNG.LoadFromFile(FileName);
 PngToBitmap(PNG,Result, False); //Convert data into bitmap
 PNG.Free;
 end;
if (Ext='.GIF') then
 begin
 Result:=TBitmap.Create;
 GiF:=TGIFImage.Create;
 GiF.LoadFromFile(FileName);
 Result.Assign(GiF);
 GiF.Free;
 end;
if (Ext='.TIF') or (Ext='.TIFF') then
 begin
  OpenTiff:=TIFFOpen(Filename,'r');
  TIFFGetField(OpenTiff,TIFFTAG_IMAGEWIDTH,@FirstPageWidth);
  TIFFGetField(OpenTiff,TIFFTAG_IMAGELENGTH,@FirstPageHeight);
  FirstPageBitmap:=TBitmap.Create;
  FirstPageBitmap.PixelFormat:=pf32bit;
  FirstPageBitmap.Width:=FirstPageWidth;
  FirstPageBitmap.Height:=FirstPageHeight;
  TIFFReadRGBAImage(OpenTiff,FirstPageWidth,FirstPageHeight,
               FirstPageBitmap.Scanline[FirstPageHeight-1],0);
  TIFFClose(OpenTiff);
  TIFFReadRGBAImageSwapRB(FirstPageWidth,FirstPageheight,
               FirstPageBitmap.Scanline[FirstPageHeight-1]);
  Result:=FirstPageBitmap;
 end;
end;

procedure SavePicture(FileName:String; Bitmap:TBitmap; CompressionValue:Integer);
var EXT:String;
 jpg:TJPEGImage;
 Png:TPngObject;
begin
EXT:=UpperCase(ExtractFileExt(FileName));
if (EXT='.BMP') then
 begin
 Bitmap.SaveToFile(FileName);
 end;
if (EXT='.JPEG') or (EXT='.JPG') then
 begin
 jpg:=TJPEGImage.Create;
 jpg.Assign(Bitmap);
 jpg.CompressionQuality:=CompressionValue;
 jpg.Compress;
 jpg.JPEGNeeded;
 jpg.SaveToFile(FileName);
 jpg.Free;
 end;
if (EXT='.PNG') then
 begin
  Png:=TPNGObject.Create;
  BitmapToPng(Bitmap, Png, False);
  Png.SaveToFile(FileName);
  Png.Free;
 end;
end;

procedure TFormSaveImage.FormCreate(Sender: TObject);
begin
  DoubleBuffered:=True;
  FPng:=TPNGObject.Create;
  FBitmap:=TBitmap.Create;
end;

procedure TFormSaveImage.FormShow(Sender: TObject);
var EXT:String;
 Tmp:Real;
 Tmp_XForm:TXForm;
begin

if FBitmap=nil then FBitmap:=TBitmap.Create;
FBitmap.Width:=Form1.PaintBox1.ClientWidth;
FBitmap.Height:=Form1.PaintBox1.ClientHeight;
FBitmap.PixelFormat:=pf32bit;
FBitmap.Canvas.Brush.Color:=clWhite;
FBitmap.Canvas.FillRect(FBitmap.Canvas.ClipRect);

Form1.DeSelect;
Form1.Canvas:=FBitmap.Canvas;
Tmp_XForm:=Form1.XForm;
Tmp:=Form1.Zoom;
Form1.btnZoomZoomClick(Form1.btnZoomZoom);
Form1.Zoom:=Form1.Zoom*1.1;
Form1.Repaint;
Form1.Canvas:=Form1.PaintBox1.Canvas;
Form1.Zoom:=Tmp;
Form1.XForm:=Tmp_XForm;
Form1.Refresh;

EXT:=UpperCase(ExtractFileExt(FileName));
if (EXT='.JPEG') or (EXT='.JPG') then
    CompressPanel.Visible:=True
    else
    CompressPanel.Visible:=False;
if (EXT='.PNG') then
    TransperentPanel.Visible:=True
    else
    begin
    TransperentPanel.Visible:=False;
    end;

ChangeFormat;
refresh;
end;

procedure TFormSaveImage.SavePicture;
var
 Bitmap:TBitmap;
begin
 Bitmap:=TBitmap.Create;
 PngToBitmap(Fpng, Bitmap, False);
 UImageSave.SavePicture(FileName,Bitmap,sbarCompress.Position);
 Bitmap.Free;
end;

procedure TFormSaveImage.FormatPanelClick(Sender: TObject);
begin
ChangeFormat;
pnlPreview.Repaint;
end;

procedure TFormSaveImage.sbarCompressChange(Sender: TObject);
begin
ChangeFormat;
pnlPreview.Repaint;
end;

procedure TFormSaveImage.btnOkClick(Sender: TObject);
begin
SavePicture;
Close;
end;

procedure TFormSaveImage.pnlPreviewCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
 PaintBox1.Width:=NewWidth;
 PaintBox1.Height:=NewHeight;
 PaintBox1.Repaint;
end;

procedure TFormSaveImage.PaintBox1Paint(Sender: TObject);
begin
  FPng.Draw(PaintBox1.Canvas, PaintBox1.ClientRect);
end;

procedure TFormSaveImage.FormDestroy(Sender: TObject);
begin
  FPng.Free;
end;

procedure TFormSaveImage.ColorBox1Change(Sender: TObject);
begin
  ChangeFormat;
  pnlPreview.Repaint;
end;

procedure TFormSaveImage.ChangeFormat;
var  jpg:TJPEGImage;
  Stream:TMemoryStream;
  Bp:TBitmap;
begin
Bp:=TBitmap.Create;
Bp.PixelFormat:=pf32bit;
Bp.Width:=FBitmap.Width;
Bp.Height:=FBitmap.Height;
Move(FBitmap.ScanLine[FBitmap.Height-1]^, Bp.ScanLine[FBitmap.Height-1]^,4*FBitmap.Width*FBitmap.Height);

if FormatPanel.Visible then
  begin
  case FormatPanel.ItemIndex of
  0: Bp.PixelFormat:=pf32bit;
  1: Bp.PixelFormat:=pf16bit;
  2: Bp.PixelFormat:=pf8bit;
  3: Bp.PixelFormat:=pf1bit;
  end;
  end;

if TransperentPanel.Visible then
  begin
  if TransperentCheck.Checked then
     bp.TransparentColor:=ColorBox1.Selected;
  end;

if CompressPanel.Visible then
 begin
 lblCompress.Caption:=Format('%d %%',[sbarCompress.Position]);
 jpg:=TJPEGImage.Create;
 jpg.Assign(bp);
 jpg.CompressionQuality:=sbarCompress.Position;
 jpg.Compress;
 jpg.JPEGNeeded;
 Stream:=TMemoryStream.Create;
 jpg.SaveToStream(Stream);
 Stream.Position:=0;
 jpg.LoadFromStream(Stream);
 bp.Assign(jpg);
 Stream.Free;
 jpg.Free;
 end;
bp.PixelFormat:=pf32bit;
if TransperentPanel.Visible then
  begin
  if TransperentCheck.Checked then
     SetTransparentColor(bp, FPNG, ColorBox1.Selected)
     else BitmapToPng(bp,FPng, True);
  end else
  BitmapToPng(bp,FPng, True);

bp.Free;
pnlPreview.repaint;
end;

end.
