unit UVariantHelp;

interface

type
  IVariant=interface(IInterface)
    procedure SetValue(const Value: Variant);
    function GetValue: Variant;
    property Value:Variant read GetValue write SetValue;
  end;
  TVariant= Class(TInterfacedObject, IVariant)
  private
    FValue:Variant;
    procedure SetValue(const Value: Variant);
    function GetValue: Variant;
  protected
  public
    constructor Create;
    destructor Destroy; 
    property Value:Variant read GetValue write SetValue;
  published

  end;

implementation

{ TVariant }

constructor TVariant.Create;
begin
  inherited;
  Self._AddRef;
end;

destructor TVariant.Destroy;
begin
  Self._Release;
  inherited;
end;

function TVariant.GetValue: Variant;
begin
 Result:=FValue;
end;

procedure TVariant.SetValue(const Value: Variant);
begin
  FValue := Value;
end;

end.
 