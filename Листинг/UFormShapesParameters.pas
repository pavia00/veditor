unit UFormShapesParameters;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,
  Widgets, UDefShapeProperty, Grids;

type
  TFormShapesParameters = class(TForm)
    lblAddress: TLabel;
    edtAddress: TEdit;
    lblDecimalName: TLabel;
    edtDecimalName: TEdit;
    lblName: TLabel;
    edtName: TEdit;
    btnDefProperty: TSpeedButton;
    gridPapametrs: TStringGrid;
    procedure btnDefPropertyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtAddressChange(Sender: TObject);
    procedure edtDecimalNameChange(Sender: TObject);
    procedure edtNameChange(Sender: TObject);
    procedure gridPapametrsSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
    procedure gridPapametrsEnter(Sender: TObject);
    procedure gridPapametrsExit(Sender: TObject);
  private
    procedure doUpdate;
    procedure WmInvalidate(var Message: TMessage); message WM_INVALIDATE;
    { Private declarations }
  public
    Shape:IShape;
    { Public declarations }
  end;

var
  FormShapesParameters: TFormShapesParameters;

implementation

{$R *.dfm}

procedure TFormShapesParameters.btnDefPropertyClick(Sender: TObject);
begin
 DefShapeProperty.Shape:=Shape;
 DefShapeProperty.Show;
end;

function CountAnProtection(const value:IShapeParameters):Integer;
var i:Integer;
begin
Result:=0;
for i:=0 to value.Count-1 do
  begin
  if value.Item[i].Protection=False then  inc(Result);
  end;
end;

procedure TFormShapesParameters.FormShow(Sender: TObject);
var i,j:Integer;
begin


edtAddress.Text:=Shape.Parameters.Value['����������� �����������'];
edtDecimalName.Text:=Shape.Parameters.Value['����������� �����'];
edtName.Text:=Shape.Parameters.Value['������������'];

gridPapametrs.Cells[0,0]:='��������';
gridPapametrs.Cells[1,0]:='��������';
gridPapametrs.Cells[2,0]:='�����������';

if CountAnProtection(Shape.Parameters)=0 then
  begin
  gridPapametrs.RowCount:=2;
  gridPapametrs.Cells[0,1]:='';
  gridPapametrs.Cells[1,1]:='';
  gridPapametrs.Cells[2,1]:='';
  end else gridPapametrs.RowCount:=CountAnProtection(Shape.Parameters)+1;

j:=1;
for i:=0 to Shape.Parameters.Count-1 do
 if Shape.Parameters.Item[i].Protection=False then
  begin
  gridPapametrs.Cells[0,J]:=Shape.Parameters.Item[i].NickName;
  gridPapametrs.Cells[1,J]:=Shape.Parameters.Item[i].Value;
  gridPapametrs.Cells[2,J]:=Shape.Parameters.Item[i].UnitStr;
  inc(J);
  end;
end;

procedure TFormShapesParameters.doUpdate;
var
   Msg: TMessage;
begin
  Msg.Msg := WM_INVALIDATE;
  Msg.WParam := 1;
  Msg.LParam := Longint(Self);
  Msg.Result := 0;
  GetParentForm(self).Broadcast(Msg);
end;

procedure TFormShapesParameters.WmInvalidate(var Message: TMessage);
begin
  FormShow(Self);
end;

procedure TFormShapesParameters.edtAddressChange(Sender: TObject);
begin
Shape.Parameters.Value['����������� �����������']:=edtAddress.Text;
end;

procedure TFormShapesParameters.edtDecimalNameChange(Sender: TObject);
begin
Shape.Parameters.Value['����������� �����']:=edtDecimalName.Text;
end;

procedure TFormShapesParameters.edtNameChange(Sender: TObject);
begin
Shape.Parameters.Value['������������']:=edtName.Text;
end;

procedure TFormShapesParameters.gridPapametrsSetEditText(Sender: TObject;
  ACol, ARow: Integer; const Value: String);
begin
Shape.Parameters.Value[ gridPapametrs.Cells[0,ARow]]:=Value;
end;

procedure TFormShapesParameters.gridPapametrsEnter(Sender: TObject);
begin
if gridPapametrs.Cells[0,gridPapametrs.Selection.Top]<>'' then
  gridPapametrs.Options:=gridPapametrs.Options-[goRowSelect];
end;

procedure TFormShapesParameters.gridPapametrsExit(Sender: TObject);
begin
  gridPapametrs.Options:=gridPapametrs.Options+[goRowSelect];
end;

end.
