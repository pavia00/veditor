unit Unit1;
//[!] ��������� ������� ���������.
// ��������� �� ������������ �������� LineControl, ������ ����� ����������.
// XForm - ������� ���������
// UVFF �������� ���������� ���������.
// + png ����������
// + �������� �������� ���������� �������� ����������.
// + �������� � ���������� �������� ������� �� ������.
// + ������ ����� ����� ����� ������� ������� Del
// + � ������� �������� �������� �� ������� ����������.
// + �������� �������� �������� �����
// + �������� ���������� �������� PEN
//+  ������������� UShapeProp Params � ��������� caption � name � �����.

{
DistanceSpline ���������.
� ��� ���� � ��� �� �����.
 Bezier(t)->Polynom(t)
 Polynom1(t)=Polynom(t)-P
 r^2(t)=Polynom1^2_x(t)+Polynom1^2_y (t)=Polynom2(t)
 ����������� ����������� ���� ����� dr^2/dt=0
 dr^2/dt=Polynom3(t)
 �������� ������� 5 �������.
 ���������� �������.
 ����� ����� SVD ������� ����������� �����.
http://mathworld.wolfram.com/PolynomialRoots.html
 �� ���� ������� ��� �������.

 ����� ����������� ����� � �����. ����������� ��� � �� ������� 0<=t<=1
 ����� � ��������.
 https://pdfs.semanticscholar.org/6b2d/950c7f57573645fa52a321228e48eafc7de5.pdf
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Math, ExtDlgs, Menus,
  // ������� ����������� ������
  jpeg, Geometrs, Widgets, UVFF,
  // ��������� ������
  pngimage, GIFImage, LibTiffDelphi, XPMan;

type
  TEditorState=(esSelection, esNewLine, esNewSpline, esMoveShape, esEditShape, esMoveSheet,
    esCancel);
const
  esZeroPoint=0;
  esOnePoint=1;
  esTwoPoint=2;
  esThreePoint=3;
  esFourPoint=4;
  esNoneSelection=0;
  esPointSelection=1;
  esRectSelection=2;

  ColorTable:array [0..3] of TColor= (clBlack, clRed, clGreen, clBlue);
//  ColorTable:array [0..3] of TColor= ($FF000000, $FF0000FF, $FF00FF00, $FFFF0000);
  SelectEps:Integer=7; // ������ �� ���������.

type

  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    mnuFile: TMenuItem;
    mnuEdit: TMenuItem;
    mnuHelp: TMenuItem;
    mnuInsertImage: TMenuItem;
    OpenPictureDialog1: TOpenPictureDialog;
    PopupMenu1: TPopupMenu;
    mnuCopy: TMenuItem;
    mnuPaste: TMenuItem;
    mnuDelete: TMenuItem;
    mnuSplit3: TMenuItem;
    mnuAbove: TMenuItem;
    mnuAboveAll: TMenuItem;
    mnuBelow: TMenuItem;
    mnuBelowAll: TMenuItem;
    mnuSplit2: TMenuItem;
    mnuDoGroup: TMenuItem;
    mnuDoUngroup: TMenuItem;
    mnuChangeZOrder: TMenuItem;
    mnuSplit1: TMenuItem;
    mnuProperty: TMenuItem;
    btnSpline: TButton;
    mnuView: TMenuItem;
    mnuWindowSizes: TMenuItem;
    mnuSave: TMenuItem;
    mnuLoad: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    procedure mnuInsertImageClick(Sender: TObject);
    procedure mnuPropertyClick(Sender: TObject);
    procedure btnSplineClick(Sender: TObject);
    procedure mnuDoGroupClick(Sender: TObject);
    procedure mnuDoUngroupClick(Sender: TObject);
    procedure mnuAboveClick(Sender: TObject);
    procedure mnuBelowClick(Sender: TObject);
    procedure mnuAboveAllClick(Sender: TObject);
    procedure mnuBelowAllClick(Sender: TObject);
    procedure mnuWindowSizesClick(Sender: TObject);
    procedure mnuSaveClick(Sender: TObject);
    procedure mnuLoadClick(Sender: TObject);
  published
    Panel1: TPanel;
    btnLine: TButton;
    PaintBox1: TPaintBox;
    ComboBox1: TComboBox;
    btnMove: TButton;
    btnDelete: TButton;
    edtZoom: TEdit;
    btnZoomZoom: TButton;
    procedure btnLineClick(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure NewLineMouseUp(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure NewSplineMouseUp(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure SelectionMouseUp(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure MoveShapeMouseUp(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure EditShapeMouseUp(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure MoveSheetMouseUp(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure CancelMouseUp(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure SelectionMouseDown(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure DrawRect(Rect:TRectControl);
    procedure DrawLine(Line:TLineControl);
    procedure DrawPolyLine(PolyLine: TPolyLineControl);
    procedure DrawSpline(SplineControl: TBezierControl);
    procedure DrawImage(ImageControl:TImageControl);
    procedure DrawGroup(GroupControl:TGroupControl);
    procedure DrawPoint(Point:TPoint);
    procedure DrawSelectedFrame(GroupControl:TGroupControl);
    procedure SelectionWidgetDraw(SelectionWidget:TRectControl);
    procedure PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
       Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure btnMoveClick(Sender: TObject);
    function  CrossByPoint(Point: TPoint; Selected:Boolean):Boolean;
    function  SelectByPoint(Point:TPoint):boolean;
    function  SelectByRect(Rect:Trect):boolean;
    function  SelectHandlePoint(Point:TPoint):boolean;
    procedure BoxSelectionMove(Rect:TRect);
    procedure DeSelect;
    procedure DeleteSelected;
    procedure UpdatePanel;
    procedure SelectionChanePen(pen:TPen);
    procedure MovePreviewLine(P:TPoint);
    procedure MovePreviewSpline(P:TPoint);
    procedure MoveSelected(X,Y:integer);
    procedure MovePoint(X,Y:integer);
    procedure MoveSheet(X, Y: integer);
    procedure MoveCancel(X, Y: integer);
    procedure btnDeleteClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtZoomKeyPress(Sender: TObject; var Key: Char);
    procedure edtZoomChange(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure ProjectToSheet(var x,y:Integer);
    procedure ProjectToCanvas(var x,y:Integer);
    procedure btnZoomZoomClick(Sender: TObject);
    function  AABBRect(Selected:Boolean):TRect;
  private
    FState: TEditorState;
    TmpState:TEditorState;
    Substate:Integer;
    PreviewLine:TLineControl;
    PreviewSpline:TBezierControl;
    ShiftBase:TPoint;
    SheetShiftBase:TPoint;
    HandleControl:THandleControl;
    EditPoint:IEditPoint;
    SelectionWidget:TRectControl;
    FZoom: Real;
    MousePos:TPoint;
    procedure SetZoom(const Value: Real);
    procedure SetState(const Value: TEditorState);
    property State:TEditorState read FState write SetState;
    function ToGroup:TGroupControl;
    function DoGroup:TGroupControl;
    procedure UnGroup(value:TGroupControl);
    function GetShapes(Index: Integer): IShape;
    procedure SetShapes(Index: Integer; const Value: IShape);
    procedure HandleControlBinded(HandleControl:THandleControl);
  public
    XForm:TXForm;
    Canvas:TCanvas;
    GroupControl:TGroupControl;
    SelectedGroup:TGroupControl;
    function ShapesCount:Integer;
    property Shapes[Index: Integer]:IShape read GetShapes write SetShapes;
    property Zoom:Real read FZoom write SetZoom;
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses UFormShapesParameters, UShapeSize, UImageSave;

{$R *.dfm}

procedure TForm1.btnLineClick(Sender: TObject);
begin
State:=esNewLine;
SubState:=esZeroPoint;
end;

procedure TForm1.btnSplineClick(Sender: TObject);
begin
State:=esNewSpline;
SubState:=esZeroPoint;
end;

procedure TForm1.btnMoveClick(Sender: TObject);
begin
State:=esSelection;
SubState:=esNoneSelection;
end;

procedure TForm1.PaintBox1Paint(Sender: TObject);
var
 i:Integer;
begin
  with PaintBox1 do
    begin
    SetGraphicsMode(canvas.Handle, GM_ADVANCED);
    ModifyWorldTransform(canvas.handle, XForm, MWT_IDENTITY);   // ��� MWT_IDENTITY , XForm ������������
    ModifyWorldTransform(canvas.handle, XForm, MWT_LEFTMULTIPLY);
    SetWorldTransform(canvas.handle, XForm);
    end;

if State=esMoveShape then
   begin
   for i:=0 to ShapesCount-1 do
     if not Shapes[i].Selected then
        Shapes[i].Draw;
   for i:=0 to ShapesCount-1 do
     if Shapes[i].Selected then
        Shapes[i].Draw;
   end else
   begin
   for i:=0 to ShapesCount-1 do
        Shapes[i].Draw;
   end;
   SelectedGroup.Draw;
if (State=esNewLine) and (SubState=esOnePoint) then
   PreviewLine.Draw;
if (State=esNewSpline) and (SubState>=esOnePoint) then
   PreviewSpline.Draw;

if (State=esSelection) and (SubState=esRectSelection) then
   SelectionWidgetDraw(SelectionWidget);
end;

procedure TForm1.NewLineMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var Line:TLineControl;
var P0, P1: TPoint;
begin
case SubState of
esZeroPoint:
  begin
  DeSelect;
  P0:=Point(X,Y);
  PreviewLine.Segment.P0:=P0;
  PreviewLine.Segment.P1:=P0;
  PreviewLine.Selected:=True;
  SubState:= esOnePoint;
  HandleControl.Binded(PreviewLine);
  end;
esOnePoint:
  begin
  P1:=Point(X,Y);
  PreviewLine.Segment.P1:=P1;
  SubState:=esZeroPoint;
  with PreviewLine.Segment do
    Line:=TLineControl.Create(P0,P1, PreviewLine.Pen);
  Line.Selected:=True;
  GroupControl.AddLine(Line);
  SelectedGroup.DeleteAll;
  SelectedGroup.Add(Line);
  HandleControl.Binded(Line);
  end;
end;
end;

procedure TForm1.NewSplineMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var Spline:TBezierControl;
var P0, P1, P2, P3: TPoint;
begin
case SubState of
esZeroPoint:
  begin
  DeSelect;
  P0:=Point(X,Y);
  PreviewSpline.Spline.P0:=P0;
  PreviewSpline.Spline.P1:=P0;
  PreviewSpline.Spline.P2:=P0;
  PreviewSpline.Spline.P3:=P0;
  PreviewSpline.Selected:=True;
  SubState:= esOnePoint;
  HandleControl.Binded(PreviewSpline);
  end;
esOnePoint:
  begin
  P1:=Point(X,Y);
  PreviewSpline.Spline.P1:=P1;
  PreviewSpline.Spline.P2:=P1;
  PreviewSpline.Spline.P3:=P1;
  SubState:=esTwoPoint;
  end;
esTwoPoint:
  begin
  P2:=Point(X,Y);
  PreviewSpline.Spline.P2:=P2;
  PreviewSpline.Spline.P3:=P2;
  SubState:=esThreePoint;
  end;
esThreePoint:
  begin
  P3:=Point(X,Y);
  PreviewSpline.Spline.P3:=P3;

  SubState:=esZeroPoint;
  with PreviewSpline.Spline do
    Spline:=TBezierControl.Create(P0,P1,P2,P3, PreviewSpline.Pen);
  Spline.Selected:=True;
  GroupControl.AddSpline(Spline);
  SelectedGroup.DeleteAll;
  SelectedGroup.Add(Spline);
  HandleControl.Binded(Spline);
  end;
end;
end;

procedure TForm1.SelectionMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if SubState=esRectSelection then
     begin
       SelectByRect(Rect(ShiftBase, Point(X,Y)));
     end;
  SubState:=esNoneSelection;
  if Button=mbRight then
     begin
     PopupMenu1.Popup(MousePos.X+PaintBox1.ClientOrigin.X,MousePos.Y+PaintBox1.ClientOrigin.Y);
     end;
end;

procedure TForm1.MoveShapeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  State:=esSelection;
  SubState:=esNoneSelection;
end;

procedure TForm1.EditShapeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  State:=esSelection;
  SubState:=esNoneSelection;
end;

procedure TForm1.CancelMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DeSelect;
  State:=esSelection;
  repaint;
end;

procedure TForm1.MoveSheetMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  State:=TmpState;
end;

procedure TForm1.SelectionMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  ShiftBase:=Point(X,Y);
  SubState:=esPointSelection;
  if SelectHandlePoint(Point(x,y)) then
    begin
      State:=esEditShape;
    end else
    begin
{    if CrossByPoint(Point(x,y), True) then
       begin
       UpdatePanel;
       State:=esMoveShape;
       end else   }
       begin
       if not(ssShift in Shift) then DeSelect;
       if SelectByPoint(Point(x,y)) then
          begin
          UpdatePanel;
          State:=esMoveShape;
          end;
       end
    end;
end;

procedure TForm1.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
ProjectToSheet(x,y);
if Button=mbLeft then
   begin
   case State of
   esSelection: SelectionMouseUp(Sender, Button, Shift, X,Y);
   esNewLine:   NewLineMouseUp(Sender, Button, Shift, X,Y);
   esNewSpline: NewSplineMouseUp(Sender, Button, Shift, X,Y);
   esMoveShape: MoveShapeMouseUp(Sender, Button, Shift, X,Y);
   esEditShape: EditShapeMouseUp(Sender, Button, Shift, X,Y);
   end; // case
   end;

if Button=mbRight then
   begin
      if CrossByPoint(Point(x,y), True) then
       begin
       PopupMenu1.Popup(MousePos.X+PaintBox1.ClientOrigin.X,MousePos.Y+PaintBox1.ClientOrigin.Y);
       State:=tmpState;
       end else CancelMouseUp(Sender, Button, Shift, X,Y);
   end;

repaint;
end;

procedure TForm1.PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
PaintBox1MouseMove(Sender, Shift, X, Y);
ProjectToSheet(x,y);
if Button=mbLeft then
   begin
   case State of
   esSelection: SelectionMouseDown(Sender, Button, Shift, X,Y);
   esNewLine:;
   esNewSpline: ;
   esMoveShape:;
   esEditShape:;
   end; // case
   end;
if Button=mbRight then
   begin
     case State of
     esSelection,
     esNewLine,
     esNewSpline,
     esMoveShape,
     esEditShape:
        begin
        SheetShiftBase:=Point(MousePos.X,MousePos.Y);
        tmpState:=State;
        State:=esCancel;
        end;
     end; // case

   end;
PaintBox1.Repaint;
end;

procedure TForm1.PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
MousePos:=Point(x,y);  // ��������� ���������� ��� MouseWheel
ProjectToSheet(x,y);
case State of
esSelection: BoxSelectionMove(Rect(ShiftBase, Point(X,Y)));
esNewLine:   MovePreviewLine(Point(X,Y));
esNewSpline: MovePreviewSpline(Point(X,Y));
esMoveShape: MoveSelected(X,Y);
esEditShape: MovePoint(X,Y);
esMoveSheet: MoveSheet(MousePos.X,MousePos.Y);
esCancel:    MoveCancel(MousePos.X,MousePos.Y);
end;
PaintBox1.Repaint;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
Canvas:=PaintBox1.Canvas;
DefulteLineDraw:=DrawLine;
DefultePolyLineDraw:=DrawPolyLine;
DefulteSplineDraw:=DrawSpline;
DefulteRectDraw:=DrawRect;
DefulteImageDraw:=DrawImage;
DefulteGroupDraw:=DrawGroup;

DoubleBuffered:=True;

PreviewLine:=TLineControl.Create;
PreviewLine.pen:=TPen.Create;
PreviewLine.pen.Width:=50;
PreviewSpline:=TBezierControl.Create;
PreviewSpline.pen:=TPen.Create;
PreviewSpline.pen.Width:=50;

GroupControl:=TGroupControl.Create;
SelectedGroup:=TGroupControl.Create;
SelectedGroup.Selected:=True;
SelectedGroup.OnDraw:=DrawSelectedFrame;
HandleControl:=THandleControl.Create;
HandleControl.OnBinded:=HandleControlBinded;

SelectionWidget:=TRectControl.Create;
with SelectionWidget do
  begin
  Pen:=TPen.Create;
  Pen.Style:=psDash;
  Pen.Mode:=pmNotXor;
  Brush:=TBrush.Create;
  Brush.Style:=bsSolid;
  end;
Zoom:=1; // 100%
state:=esNewLine;
SubState:=esZeroPoint;

end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
PreviewLine.pen.Color:=ColorTable[ComboBox1.ItemIndex];
SelectionChanePen(PreviewLine.pen);
RePaint;
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key=VK_ESCAPE then
  begin
  DeSelect;
  repaint;
  State:=esSelection;
  SubState:=esNoneSelection;
  end;
if Key=VK_DELETE  then
  begin
  DeleteSelected;
  Repaint;
  end;
end;

procedure TForm1.DrawLine(Line: TLineControl);
var
 TmpPen:TPen;
begin
  TmpPen:=Canvas.Pen;
  if Line.Selected then
     begin
     Canvas.Pen.Color:=clYellow;
     Canvas.Pen.Width:=Line.Pen.Width+4;
     Canvas.MoveTo(Line.Segment.P0.X, Line.Segment.P0.Y);
     Canvas.LineTo(Line.Segment.P1.X, Line.Segment.P1.Y);
     end;
  Canvas.Pen:=Line.Pen;
  Canvas.MoveTo(Line.Segment.P0.X, Line.Segment.P0.Y);
  Canvas.LineTo(Line.Segment.P1.X, Line.Segment.P1.Y);
 if Line.Selected then
    begin
    DrawPoint(Line.Segment.P0);
    DrawPoint(Line.Segment.P1);
    end;
  Canvas.Pen:=TmpPen;
end;

procedure TForm1.DrawPolyLine(PolyLine: TPolyLineControl);
var
 TmpPen:TPen;
begin
  TmpPen:=Canvas.Pen;
  if PolyLine.Selected then
     begin
     Canvas.Pen.Color:=clYellow;
     Canvas.Pen.Width:=PolyLine.Pen.Width+4;

     Canvas.Polyline(PolyLine.PolyLine);
     end;
  Canvas.Pen:=PolyLine.Pen;
  Canvas.Polyline(PolyLine.PolyLine);
 if PolyLine.Selected then
    begin
    DrawPoint(PolyLine.PolyLine[0]);
    DrawPoint(PolyLine.PolyLine[Length(PolyLine.PolyLine)]);
    end;
  Canvas.Pen:=TmpPen;
end;

procedure TForm1.DrawSpline(SplineControl: TBezierControl);
var
 TmpPen:TPen;
 Bezier:TBezier;
begin
  Bezier:=SplineToBezier(SplineControl.Spline);
  TmpPen:=Canvas.Pen;
  if SplineControl.Selected then
     begin
     Canvas.Pen.Color:=clGreen;
     Canvas.Pen.Width:=1;
     Canvas.Pen.Style:=psSolid;
     Canvas.Brush.Style:=bsClear;
     Canvas.Rectangle(Geometrs.AABBRect(Bezier));
     Canvas.Pen.Color:=clYellow;
     Canvas.Pen.Width:=SplineControl.Pen.Width+4;
     Canvas.Polyline([SplineControl.Spline.P0, SplineControl.Spline.P1,
                                  SplineControl.Spline.P2, SplineControl.Spline.P3]);

     Canvas.Pen.Color:=clFuchsia;
     Canvas.Pen.Width:=SplineControl.Pen.Width+2;
     Canvas.MoveTo(Bezier.B0.X, Bezier.B0.Y);
     Canvas.LineTo(Bezier.H1.X, Bezier.H1.Y);
     Canvas.MoveTo(Bezier.B3.X, Bezier.B3.Y);
     Canvas.LineTo(Bezier.H2.X, Bezier.H2.Y);


      end;
  Canvas.Pen:=SplineControl.Pen;
  Canvas.MoveTo(SplineControl.Spline.P0.X, SplineControl.Spline.P0.Y);
  Canvas.PolyBezier([Bezier.B0, Bezier.H1, Bezier.H2, Bezier.B3]);
 if SplineControl.Selected then
    begin
    DrawPoint(SplineControl.Spline.P0);
    DrawPoint(SplineControl.Spline.P1);
    DrawPoint(SplineControl.Spline.P2);
    DrawPoint(SplineControl.Spline.P3);
    end;
  Canvas.Pen:=TmpPen;
end;

procedure TForm1.DrawPoint(Point: TPoint);
var
  TmpColor:TColor;
  TmpBrushStyle:TBrushStyle;
  TmpPenMode:TPenMode;
begin
  TmpColor:=Canvas.Pen.Color;
  TmpBrushStyle:=Canvas.Brush.Style;
  TmpPenMode:=Canvas.Pen.Mode;

  Canvas.Pen.Color:=clBlack;
  Canvas.Pen.Width:=1;
  Canvas.Pen.Mode:=pmNotXor;
  Canvas.Brush.Style:=bsClear;
  Canvas.Ellipse(Point.X-HandleRadius, Point.Y-HandleRadius, Point.X+HandleRadius, Point.Y+HandleRadius);

  Canvas.Brush.Style:=TmpBrushStyle;
  Canvas.Pen.Color:=TmpColor;
  Canvas.Pen.Mode:=TmpPenMode;
end;

procedure AlphaBlend(Dest:HDC; XOriginDest, yoriginDest, WidthDest, HeightDest:Integer;
                     Src: HDC; xoriginSrc, yoriginSrc, WidthSrc, HeightSrc:Integer;
                     BlendFunction: TBlendFunction);stdcall;external 'Msimg32.dll';

procedure TForm1.DrawImage(ImageControl: TImageControl);
var
 TmpPen:TPen;
 Rect1:TRect;
 BlendFunction:TBlendFunction;
 Png:TPngObject;

begin
  TmpPen:=Canvas.Pen;
  Rect1:=Rect(ImageControl.Image.Quadrilateral.P0, ImageControl.Image.Quadrilateral.P2);
  if ImageControl.Selected then
     begin
     Canvas.Pen.Color:=clYellow;
     Canvas.Pen.Width:=Round(5*(1/Zoom));
     Canvas.Brush.Style:=bsClear;
     Canvas.Rectangle(Rect1);
     end;

  Png:=TPngObject.Create;
  BitmapToPng(ImageControl.Image.Bitmap, png, False);
  png.CreateAlpha;
  Png.Draw(Canvas, Rect1);
  png.Destroy;
  //Canvas.StretchDraw(Rect1, ImageControl.Image.Bitmap);
{  BlendFunction.BlendOp := AC_SRC_OVER; //AC_SRC_OVER; //AC_SRC_ALPHA;
  BlendFunction.BlendFlags := 0;
  BlendFunction.SourceConstantAlpha := 255;
  if ImageControl.Image.Bitmap.PixelFormat=pf32bit then
       BlendFunction.AlphaFormat := AC_SRC_NO_PREMULT_ALPHA
     else
       BlendFunction.AlphaFormat := 0;
   BlendFunction.AlphaFormat :=  AC_SRC_NO_PREMULT_ALPHA; //{AC_SRC_NO_PREMULT_ALPHA or AC_DST_NO_ALPHA or AC_DST_NO_PREMULT_ALPHA;
{
   Unit1.AlphaBlend(
      Canvas.Handle,
         Rect1.Left,Rect1.Top, Rect1.Right-Rect1.Left, Rect1.Bottom-Rect1.Top,
      ImageControl.Image.Bitmap.Canvas.Handle,
         0,0, ImageControl.Image.Bitmap.Width, ImageControl.Image.Bitmap.Height,
      BlendFunction);
}
 if ImageControl.Selected then
    begin
    DrawPoint(ImageControl.Image.Quadrilateral.P0);
    DrawPoint(ImageControl.Image.Quadrilateral.P1);
    DrawPoint(ImageControl.Image.Quadrilateral.P2);
    DrawPoint(ImageControl.Image.Quadrilateral.P3);
    end;
  Canvas.Pen:=TmpPen;
end;

procedure TForm1.SelectionWidgetDraw(SelectionWidget:TRectControl);
var TmpRect:TRect;
begin
  // ��� ������ ����� ���������� ������, ������ � ������� �����������
  ModifyWorldTransform(canvas.handle, XForm, MWT_IDENTITY);
  TmpRect:=SelectionWidget.Rect;
  with SelectionWidget do
    begin
    ProjectToCanvas(Rect.Left, Rect.Top);
    ProjectToCanvas(Rect.Right, Rect.Bottom);
    end;
  DrawRect(SelectionWidget);
  SelectionWidget.rect:=TmpRect;
  SetWorldTransform(canvas.handle, XForm);
end;

function TForm1.SelectByPoint(Point: TPoint):Boolean;
var
  i:Integer;
  d,MinD:Real;
  MinIndex:Integer;
begin
Result:=False;
if ShapesCount<>0 then
  begin
  MinD:=MaxInt;
  MinIndex:=-1;
  For i:=ShapesCount-1 downto 0 do
    begin
    d:=Shapes[i].Distance(Point);
    if (d<SelectEps) and  (d< MinD) then
      begin
      if (Shapes[i].GetPixel(Point.X, Point.Y).A<10) then continue;
      MinD:=d;
      MinIndex:=i;
      Break;
      end;
    end;
  if MinD<SelectEps then
    begin
    SelectedGroup.DeleteAll;
    Shapes[MinIndex].Selected:=True;
    SelectedGroup.Add(Shapes[MinIndex]);
    ShiftBase:=Point;
    Result:=True
    end;
  end;
end;

function TForm1.CrossByPoint(Point: TPoint; Selected:Boolean):Boolean;
var
  i:Integer;
  d,MinD:Real;
  MinIndex:Integer;
begin
Result:=False;
if ShapesCount<>0 then
  begin
  MinD:=MaxInt;
  MinIndex:=-1;
  For i:=ShapesCount-1 downto 0 do
   if (Shapes[i].Selected or (not Selected))=True then
    begin
    d:=Shapes[i].Distance(Point);
    if d< MinD then
      begin
      MinD:=d;
      MinIndex:=i;
      end;
    end;
  if MinD<SelectEps then
    begin
    ShiftBase:=Point;
    Result:=True
    end;
  end;
end;

function TForm1.SelectByRect(Rect: Trect): boolean;
  procedure NormRect(var rect:TRect);
  var tmp:TScalar;
  begin
  if Rect.Left> Rect.Right then
     begin
     tmp:=Rect.Left;
     Rect.Left:=Rect.Right;
     Rect.Right:=tmp;
     end;
  if Rect.Top> Rect.Bottom then
     begin
     tmp:=Rect.Top;
     Rect.Top:=Rect.Bottom;
     Rect.Bottom:=tmp;
     end;
  end;
var
  i:Integer;
begin
NormRect(Rect);
Result:=False;
SelectedGroup.DeleteAll;
For i:=0 to ShapesCount-1 do
 if (Shapes[i].InRect(Rect)) then
    begin
      Result:=True;
      Shapes[i].Selected:=True;
      SelectedGroup.Add(Shapes[i]);
    end;
end;

function TForm1.SelectHandlePoint(Point: TPoint): boolean;
var
  i:Integer;
begin
Result:=False;
if ShapesCount<>0 then
  begin
  For i:=0 to Length(GroupControl.Group.Shapes)-1 do
    if GroupControl.Group.Shapes[i].Selected then
    begin
    HandleControl.Binded(GroupControl.Group.Shapes[i]);
    if HandleControl.HintPoint(Point) then
       begin
       EditPoint:=HandleControl.SelectedPoint;
       FormShapeSize.Update(HandleControl);
       Result:=True;
       end;
    end;
  end;

end;

procedure TForm1.DeSelect;
var
  i:Integer;
begin
for i:=0 to ShapesCount-1 do
  Shapes[i].Selected:=False;
SelectedGroup.DeleteAll;
end;


function TForm1.AABBRect(Selected:Boolean):TRect;
begin
if Selected then Result:= SelectedGroup.AABBRect
  else Result:=GroupControl.AABBRect;
end;

procedure TForm1.UpdatePanel;
var
  i, j:Integer;
begin
With GroupControl.Group do
for i:=0 to Length(Lines)-1 do
  if Lines[i].Selected then
     begin
       for j:=Low(ColorTable) to High(ColorTable) do
         if Lines[i].Pen.Color=ColorTable[j] then
           ComboBox1.ItemIndex:=j;
     end;
end;

procedure TForm1.SelectionChanePen(pen: TPen);
var i:Integer;
begin
with GroupControl.Group do
for i:=0 to Length(Lines)-1 do
  if Lines[i].Selected then
     Lines[i].Pen.Assign(pen);
end;

procedure TForm1.MoveSelected(X, Y: integer);
var i:Integer;
  P:TPoint;
begin
p:=Point(X,Y);
for i:=0 to ShapesCount-1 do
  if Shapes[i].Selected then
    begin
     Shapes[i].ShiftTo(Sub(P,ShiftBase))
    end;
ShiftBase:=P;
FormShapeSize.Update(HandleControl);
end;

procedure TForm1.MovePreviewLine(P:TPoint);
begin
  if (SubState=esOnePoint) then
     PreviewLine.Segment.P1:=P;
  FormShapeSize.Update(HandleControl);
end;

procedure TForm1.MovePreviewSpline(P:TPoint);
begin
  case (SubState) of
  esOnePoint:
     begin
     PreviewSpline.Spline.P1:=P;
     PreviewSpline.Spline.P2:=P;
     PreviewSpline.Spline.P3:=P;
     end;
  esTwoPoint:
     begin
     PreviewSpline.Spline.P2:=P;
     PreviewSpline.Spline.P3:=P;
     end;
  esThreePoint:
     begin
     PreviewSpline.Spline.P3:=P;
     end;
  end;
  FormShapeSize.Update(HandleControl);
end;

procedure TForm1.MovePoint(X, Y: integer);
begin
EditPoint.X:=EditPoint.X+(X-ShiftBase.X);
EditPoint.Y:=EditPoint.Y+(Y-ShiftBase.Y);
ShiftBase:=Point(x,y);
FormShapeSize.Update(HandleControl);
end;

procedure TForm1.MoveSheet(X, Y: integer);
begin
xForm.eDx:=xForm.eDx+X-SheetShiftBase.X;
xForm.eDy:=xForm.eDy+Y-SheetShiftBase.Y;
SheetShiftBase:=Point(x,y);
end;

procedure TForm1.MoveCancel(X, Y: integer);
begin
if Distance(Point(X,Y),SheetShiftBase)>=SelectEps then
   begin
   State:=esMoveSheet;
   end;
end;

procedure TForm1.btnDeleteClick(Sender: TObject);
begin
DeleteSelected;
Repaint;
end;

procedure TForm1.DeleteSelected;
var i,j:Integer;
begin
SelectedGroup.DeleteAll;
with GroupControl.Group do
  begin
  j:=0;
  for i:=0 to Length(Lines)-1 do
    begin
    if not Lines[i].Selected then
     begin
     Lines[j]:=Lines[i];
     Inc(j);
     end else
     Lines[i]:=Nil;  // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(Lines, J);

  j:=0;
  for i:=0 to Length(PolyLines)-1 do
    begin
    if not PolyLines[i].Selected then
     begin
     PolyLines[j]:=PolyLines[i];
     Inc(j);
     end else
     PolyLines[i]:=Nil;  // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(PolyLines, J);

  j:=0;
  for i:=0 to Length(Splines)-1 do
    begin
    if not Splines[i].Selected then
     begin
     Splines[j]:=Splines[i];
     Inc(j);
     end else
     Splines[i]:=Nil;  // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(Splines, J);

  j:=0;
  for i:=0 to Length(Images)-1 do
    begin
    if not Images[i].Selected then
     begin
     Images[j]:=Images[i];
     Inc(j);
     end else Images[i]:=Nil; // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(Images, J);

  j:=0;
  for i:=0 to Length(Groups)-1 do
    begin
    if not Groups[i].Selected then
     begin
     Groups[j]:=Groups[i];
     Inc(j);
     end else Groups[i]:=Nil; // ��� ������������ �������� ����� Free ����������� nil
    end;
  SetLength(Groups, J);

  j:=0;
  for i:=0 to Length(Shapes)-1 do
    if not Shapes[i].Selected then
     begin
     Shapes[j]:=Shapes[i];
     Inc(j);
     end else Shapes[i]:=nil; // ��� ������������ �������� ����� Free ����������� nil
  SetLength(Shapes, J);
  end;
end;

procedure TForm1.BoxSelectionMove(Rect: TRect);
begin
if SubState<>esNoneSelection then
 if Norm(Sub(Rect.BottomRight, Rect.TopLeft))>=SelectEps then
   begin
   SelectionWidget.Rect:=Rect;
   Windows.SetCursor(Screen.Cursors[crCross]);
   SubState:=esRectSelection;
   end else
   begin
   SubState:=esPointSelection;
   end;

end;

procedure TForm1.DrawRect(Rect: TRectControl);
var
 TmpPen:TPen;
 TmpBrush:TBrush;
begin
 TmpPen:=Canvas.Pen;
 TmpBrush:=Canvas.Brush;
 Canvas.Pen:=Rect.Pen;
 Canvas.Brush:=Rect.Brush;

 Canvas.Rectangle(Rect.Rect);

 Canvas.Pen:=TmpPen;
 Canvas.Brush:=TmpBrush;
end;

procedure TForm1.DrawGroup(GroupControl: TGroupControl);
var i:Integer;
var
 TmpPen:TPen;
 Quadrilateral:TQuadrilateral;
begin
  TmpPen:=Canvas.Pen;
  if GroupControl.Selected then
     begin
      Canvas.Pen.Color:=clGreen;
      Canvas.Pen.Width:=Round(1*(1/Zoom));
      Canvas.Pen.Style:=psDash;
      Canvas.Brush.Style:=bsClear;
      Canvas.Rectangle(GroupControl.AABBRect);
     end;

  For i:=0 to GroupControl.ShapesCount-1 do
    GroupControl.Group.Shapes[i].Draw;

 if GroupControl.Selected then
    begin
    Quadrilateral:=RectToQuadrilateral(GroupControl.AABBRect);
    DrawPoint(Quadrilateral.P0);
    DrawPoint(Quadrilateral.P1);
    DrawPoint(Quadrilateral.P2);
    DrawPoint(Quadrilateral.P3);
    end;
  Canvas.Pen:=TmpPen;

end;

procedure TForm1.edtZoomKeyPress(Sender: TObject; var Key: Char);
var Edit:TEdit;
begin
Edit:=Sender as TEdit;
   if not (Key in [#8, #13,'0'..'9', ',','.', '+','-']) then
      key:=#0
      else
      Edit.Modified:=True;
end;

procedure TForm1.edtZoomChange(Sender: TObject);
var Edit:TEdit;
Value:Double;
begin
Edit:=Sender as TEdit;
if Edit.Modified then
  begin
    Edit.Modified:=False;
    if not TryStrToFloat(Edit.Text, Value) then
       begin
       Edit.Undo;
       end else  Edit.Modified:=true;
  end;
if Edit.Modified then
   begin
   Zoom:=StrToFloat(Edit.Text)/100;
   Edit.Modified:=False;
   Repaint;
   end;
end;

procedure TForm1.SetZoom(const Value: Real);
begin
  FZoom := Value;
  if FZoom>32 then FZoom:=32;  // 2^N, ����� ��� �������� ����������� �������� 100 %.
  if FZoom<0.001 then FZoom:=0.001;
  edtZoom.Text:=Format('%.0f', [FZoom*100]);

  XForm.eM11:=FZoom; XForm.eM12:=0;
  XForm.eM21:=0;     XForm.eM22:=FZoom;
  HandleRadius:=Round(SelectEps*(1/Zoom));
  if HandleRadius<7 then HandleRadius:=7;
end;

procedure TForm1.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  Z1,Z2:Real;
begin
 MousePos:=Self.MousePos;  // ��� � ������������, ������ �� ����������
 Z1:=Zoom;
 Zoom:=Zoom*power(2, WheelDelta/3/120);
 Z2:=Zoom;
 XForm.eDx:=XForm.eDx-MousePos.X;
 XForm.eDy:=XForm.eDy-MousePos.Y;
 XForm.eDx:=(XForm.eDx)/Z1*Z2;
 XForm.eDy:=(XForm.eDy)/Z1*Z2;
 XForm.eDx:=XForm.eDx+MousePos.X;
 XForm.eDy:=XForm.eDy+MousePos.Y;
 Repaint;
end;

procedure TForm1.ProjectToSheet(var x, y: Integer);
begin
x:=Round((x-XForm.eDx) / Zoom);
y:=Round((y-XForm.eDy) / Zoom);
end;

procedure TForm1.ProjectToCanvas(var x, y: Integer);
begin
x:=Round(x*Zoom+XForm.eDx);
y:=Round(y*Zoom+XForm.eDy);
end;


procedure TForm1.btnZoomZoomClick(Sender: TObject);
var
 Rect:TRect;
begin
 Rect:=AABBRect(False);
 if (Rect.Right-Rect.Left=0) or (Rect.Bottom-Rect.Top=0) then
     Zoom:=1
     else
     begin
        Zoom:=min((Canvas.ClipRect.Right-Canvas.ClipRect.Left)/(Rect.Right-Rect.Left),
                  (Canvas.ClipRect.Bottom-Canvas.ClipRect.Top)/(Rect.Bottom-Rect.Top));
     end;
 xForm.eDx:=-Rect.Left*Zoom;
 xForm.eDy:=-Rect.Top*Zoom;
 repaint;
end;

procedure TForm1.SetState(const Value: TEditorState);
begin
  FState := Value;
  case Value of
  esNewLine:
    begin
    PaintBox1.Cursor:=crCross;
    Windows.SetCursor(Screen.Cursors[crCross]);
    end;
  esSelection:
    begin
    PaintBox1.Cursor:=crHandPoint;
    Windows.SetCursor(Screen.Cursors[crHandPoint]);
    end;
  esEditShape:
    begin
    PaintBox1.Cursor:=crDefault;
    Windows.SetCursor(Screen.Cursors[crDefault]);
    end;
  esMoveShape:
    begin
    PaintBox1.Cursor:=crDefault;
    Windows.SetCursor(Screen.Cursors[crDefault]);
    end;
  esMoveSheet:
    begin
    PaintBox1.Cursor:=crHandPoint;
    Windows.SetCursor(Screen.Cursors[crHandPoint]);
    end;
  end;
end;

procedure TForm1.mnuInsertImageClick(Sender: TObject);
var bp:TBitmap;
  ImageControl:TImageControl;
begin
if  OpenPictureDialog1.Execute then
    begin
      bp:=LoadImage(OpenPictureDialog1.Files[0]);
      ImageControl:=TImageControl.Create(bp);
      GroupControl.AddImage(ImageControl);
      HandleControl.Binded(ImageControl);
      repaint;
    end;
end;

function TForm1.DoGroup: TGroupControl;
var i:integer;
begin
  Result:=ToGroup;
  DeleteSelected;
  Result.DeSelect;
  GroupControl.add(Result);
  Shapes[ShapesCount-1].Selected:=True;
end;

function TForm1.ToGroup: TGroupControl;
var i:Integer;
begin
  Result:=TGroupControl.Create;
  for i:=0 to ShapesCount-1 do
     begin
       if Shapes[i].Selected then
          Result.Add(Shapes[i]);
     end;
end;

procedure TForm1.UnGroup(value: TGroupControl);
var i:Integer;
begin
  for i:=0 to Length(value.Group.Shapes)-1 do
    begin
    value.Group.Shapes[i].Selected:=True;
    GroupControl.Add(value.Group.Shapes[i]);
    end;

  i:=Length(value.Group.Shapes)-1;
  while i>=0 do
    begin
    value.Delete(i);
    dec(i);
    end;

  i:=GroupControl.IndexBy(value);
  GroupControl.Delete(i);
end;

function TForm1.ShapesCount: Integer;
begin
  Result:=Length(GroupControl.Group.Shapes);
end;

function TForm1.GetShapes(Index: Integer): IShape;
begin
 result:=GroupControl.Group.Shapes[Index];
end;

procedure TForm1.SetShapes(Index: Integer; const Value: IShape);
begin
 GroupControl.Group.Shapes[Index]:=Value;
end;

procedure TForm1.mnuDoGroupClick(Sender: TObject);
begin
  DoGroup;
end;

procedure TForm1.mnuDoUngroupClick(Sender: TObject);
var i:Integer;
begin
  for i:=Length(GroupControl.Group.Groups)-1 downto 0 do
    UnGroup(GroupControl.Group.Groups[i]);
end;

procedure TForm1.mnuAboveClick(Sender: TObject);
var I:Integer;
begin
  for i:=GroupControl.ShapesCount-2 downto 0 do
    begin
    if Ord(GroupControl.Group.Shapes[I].Selected)>Ord(GroupControl.Group.Shapes[I+1].Selected) then
     GroupControl.Swap(I, I+1);
    end;
end;


procedure TForm1.mnuBelowClick(Sender: TObject);
var I:Integer;
begin
  for i:=1 to GroupControl.ShapesCount-1 do
    begin
    if Ord(GroupControl.Group.Shapes[I-1].Selected)<Ord(GroupControl.Group.Shapes[I].Selected) then
     GroupControl.Swap(I, I-1);
    end;
end;

procedure TForm1.mnuAboveAllClick(Sender: TObject);
var I,J,N:Integer;
begin
N:=GroupControl.ShapesCount;
for I:=0 to N-2 do
  begin
  {���������� ���� ��� ���������� �������� � ���������� ����� �����.}
  for J:=0 to N-2-I do
    begin
    {���� �������, ������ ����������, �� ������ �������.}
    if Ord(GroupControl.Group.Shapes[J].Selected)>Ord(GroupControl.Group.Shapes[J+1].Selected) then
      GroupControl.Swap(J,J+1);
    end;
  end;
end;

procedure TForm1.mnuBelowAllClick(Sender: TObject);
var I,J,N:Integer;
begin
N:=GroupControl.ShapesCount;
for I:=0 to N-2 do
  begin
  {���������� ���� ��� ���������� �������� � ���������� ����� �����.}
  for J:=0 to N-2-I do
    begin
    {���� �������, ������ ����������, �� ������ �������.}
    if Ord(GroupControl.Group.Shapes[J].Selected)<Ord(GroupControl.Group.Shapes[J+1].Selected) then
      GroupControl.Swap(J,J+1);
    end;
  end;
end;

procedure TForm1.DrawSelectedFrame(GroupControl: TGroupControl);
var
 TmpPen:TPen;
 TmpBrush:TBrush;
begin
TmpPen:=PaintBox1.Canvas.Pen;
TmpBrush:=PaintBox1.Canvas.Brush;
if GroupControl.Selected then
   if GroupControl.ShapesCount>1 then
      begin
      PaintBox1.Canvas.Pen.Color:=clBlue;
      PaintBox1.Canvas.Pen.Width:=1;
      PaintBox1.Canvas.Pen.Style:=psDash;
      PaintBox1.Canvas.Brush.Style:=bsClear;
      PaintBox1.Canvas.Rectangle(AddBound(GroupControl.AABBRect, 15));
      end;
PaintBox1.Canvas.Brush:=TmpBrush;
PaintBox1.Canvas.Pen:=TmpPen;
end;

procedure TForm1.mnuPropertyClick(Sender: TObject);
begin
  FormShapesParameters.Shape:=SelectedGroup.Group.Shapes[0];
  FormShapesParameters.Show;
end;

procedure TForm1.mnuWindowSizesClick(Sender: TObject);
begin
 FormShapeSize.Top:=Screen.WorkAreaHeight-FormShapeSize.Height;
 FormShapeSize.Left:=Screen.WorkAreaWidth-FormShapeSize.Width;
 FormShapeSize.visible:=True;
end;

procedure TForm1.HandleControlBinded(HandleControl: THandleControl);
begin
  FormShapeSize.Update(HandleControl);
end;

procedure TForm1.mnuSaveClick(Sender: TObject);
var EXT:String;
begin
if SaveDialog1.Execute then
  begin
  if  FileExists(SaveDialog1.FileName) then
    if IDYES<>Application.MessageBox(PAnsiChar('�� ������ ������������ ����:"'+SaveDialog1.FileName+'"?'), '��������������', MB_YESNO) then
      exit;
  EXT:=UpperCase(ExtractFileExt(SaveDialog1.FileName));
  if EXT='VFF' then VFFSave(SaveDialog1.FileName, Self.GroupControl)
     else
     begin
       FormSaveImage.FileName:=SaveDialog1.FileName;
       FormSaveImage.Show;
     end;
  end;
end;

procedure TForm1.mnuLoadClick(Sender: TObject);
begin
GroupControl.DeleteAll;
if OpenDialog1.Execute then
  begin
  if not FileExists(OpenDialog1.FileName) then
     Application.MessageBox(PAnsiChar('������ ���� �� ����������:"'+OpenDialog1.FileName+'"!'), '������')
     else if not VFFLoad(OpenDialog1.FileName, GroupControl) then
       begin
       Application.MessageBox(PAnsiChar('������������ ��������� �����:"'+OpenDialog1.FileName+'"!'), '������');
       end;
  end;

end;

end.
