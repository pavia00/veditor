object Form1: TForm1
  Left = 187
  Top = 233
  Width = 870
  Height = 551
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Visible = True
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnMouseWheel = FormMouseWheel
  PixelsPerInch = 96
  TextHeight = 13
  object PaintBox1: TPaintBox
    Left = 99
    Top = 0
    Width = 755
    Height = 492
    Cursor = crCross
    Align = alClient
    OnMouseDown = PaintBox1MouseDown
    OnMouseMove = PaintBox1MouseMove
    OnMouseUp = PaintBox1MouseUp
    OnPaint = PaintBox1Paint
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 99
    Height = 492
    Align = alLeft
    AutoSize = True
    Locked = True
    ParentBackground = False
    TabOrder = 0
    object btnLine: TButton
      Left = 9
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Line'
      TabOrder = 0
      OnClick = btnLineClick
      OnKeyUp = FormKeyUp
    end
    object ComboBox1: TComboBox
      Left = 1
      Top = 72
      Width = 97
      Height = 21
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 1
      Text = 'Black'
      OnChange = ComboBox1Change
      Items.Strings = (
        'Black'
        'Red'
        'Green'
        'Blue')
    end
    object btnMove: TButton
      Left = 9
      Top = 104
      Width = 75
      Height = 25
      Caption = 'Move'
      TabOrder = 2
      OnClick = btnMoveClick
      OnKeyUp = FormKeyUp
    end
    object btnDelete: TButton
      Left = 9
      Top = 136
      Width = 75
      Height = 25
      Caption = 'Delete'
      TabOrder = 3
      OnClick = btnDeleteClick
      OnKeyUp = FormKeyUp
    end
    object edtZoom: TEdit
      Left = 9
      Top = 176
      Width = 73
      Height = 21
      TabOrder = 4
      Text = '100'
      OnChange = edtZoomChange
      OnKeyPress = edtZoomKeyPress
    end
    object btnZoomZoom: TButton
      Left = 9
      Top = 200
      Width = 75
      Height = 25
      Caption = 'ZoomZoom'
      TabOrder = 5
      OnClick = btnZoomZoomClick
      OnKeyUp = FormKeyUp
    end
  end
  object btnSpline: TButton
    Left = 8
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Spline'
    TabOrder = 1
    OnClick = btnSplineClick
    OnKeyUp = FormKeyUp
  end
  object MainMenu1: TMainMenu
    Left = 8
    Top = 224
    object mnuFile: TMenuItem
      Caption = #1060#1072#1081#1083
      object mnuSave: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        OnClick = mnuSaveClick
      end
      object mnuLoad: TMenuItem
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
        OnClick = mnuLoadClick
      end
    end
    object mnuEdit: TMenuItem
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      object mnuInsertImage: TMenuItem
        Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
        OnClick = mnuInsertImageClick
      end
    end
    object mnuView: TMenuItem
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      object mnuWindowSizes: TMenuItem
        Caption = #1056#1072#1079#1084#1077#1088#1099
        OnClick = mnuWindowSizesClick
      end
    end
    object mnuHelp: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1082#1072
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 
      'All (*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf;*.tif;*.tiff;*.png;*.g' +
      'if)|*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf;*.tif;*.tiff;*.png; *.g' +
      'if|JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg' +
      '|Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*' +
      '.emf)|*.emf|Metafiles (*.wmf)|*.wmf|Tiff Image File (*.tif;*.tif' +
      'f)|*.tif;*.tiff|Portable Network Graphics(*.png)|*.png|Graphics ' +
      'Interchange Format|*.gif'
    Left = 120
  end
  object PopupMenu1: TPopupMenu
    Left = 40
    Top = 224
    object mnuProperty: TMenuItem
      Caption = #1057#1074#1086#1081#1089#1090#1074#1072
      OnClick = mnuPropertyClick
    end
    object mnuSplit1: TMenuItem
      Caption = '-'
    end
    object mnuCopy: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
    end
    object mnuPaste: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
    end
    object mnuDelete: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
    end
    object mnuSplit2: TMenuItem
      Caption = '-'
    end
    object mnuDoGroup: TMenuItem
      Caption = #1043#1088#1091#1087#1087#1080#1088#1086#1072#1090#1100
      OnClick = mnuDoGroupClick
    end
    object mnuDoUngroup: TMenuItem
      Caption = #1056#1072#1079#1075#1088#1091#1087#1087#1080#1088#1086#1074#1072#1090#1100
      OnClick = mnuDoUngroupClick
    end
    object mnuSplit3: TMenuItem
      Caption = '-'
    end
    object mnuChangeZOrder: TMenuItem
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1087#1086#1088#1103#1076#1086#1082
      object mnuAbove: TMenuItem
        Caption = #1042#1099#1096#1077
        OnClick = mnuAboveClick
      end
      object mnuAboveAll: TMenuItem
        Caption = #1042#1099#1096#1077' '#1074#1089#1077#1093
        OnClick = mnuAboveAllClick
      end
      object mnuBelow: TMenuItem
        Caption = #1053#1080#1078#1077
        OnClick = mnuBelowClick
      end
      object mnuBelowAll: TMenuItem
        Caption = #1053#1080#1078#1077' '#1074#1089#1077#1093
        OnClick = mnuBelowAllClick
      end
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.vff'
    Filter = 'Vector File Format|*.vff'
    Left = 8
    Top = 256
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.vff'
    Filter = 
      'Vector File Format|*.vff|Bitmap|*.bmp|Portable Network Graphics|' +
      '*.png|JPEG File Format|*.jpg'
    Left = 40
    Top = 256
  end
end
