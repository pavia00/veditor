unit UVFF;
// Vector File Format

interface

uses Types, SysUtils, MSXML2_TLB, Forms, Graphics, Classes,
Widgets,Geometrs, EncdDecd;

const ProgrammName='��������� ��������';
const ProgrammVersion='1.0';

function VFFSave(FileName:String; GroupControl:TGroupControl):boolean;
function VFFLoad(FileName:String; var GroupControl:TGroupControl):boolean;

implementation

function GenManifestNode(XMLDoc:IXMLDomDocument; ProgrammName:String; Version:String): IXMLDOMNode;
var LineControl:TLineControl;
 Attribut:IXMLDOMNode;
 SubNode:IXMLDOMNode;
begin
  Result:=XMLDoc.createElement('Manifest');
  Attribut:=XMLDoc.createAttribute('encode');
  Attribut.nodeValue:= 'UTF-8';
  Result.attributes.setNamedItem(Attribut);
  SubNode:= XMLDoc.createElement('ProgrammName');
  SubNode.text:= ProgrammName;
  Result.appendChild(SubNode);
  SubNode:= XMLDoc.createElement('Version');
  SubNode.text:= Version;
  Result.appendChild(SubNode);
end;

function GenPointNode(XMLDoc:IXMLDomDocument; P:TPoint; Number:Integer): IXMLDOMNode;
var LineControl:TLineControl;
 Attribut:IXMLDOMNode;
 SubNode:IXMLDOMNode;
begin
  Result:=XMLDoc.createElement('Point');
  Attribut:=XMLDoc.createAttribute('Num');
  Attribut.nodeValue:= Number;
  Result.attributes.setNamedItem(Attribut);
  SubNode:= XMLDoc.createElement('X');
  SubNode.text:= IntToStr(P.X);
  Result.appendChild(SubNode);
  SubNode:= XMLDoc.createElement('Y');
  SubNode.text:= IntToStr(P.Y);
  Result.appendChild(SubNode);
end;

function GenPenNode(XMLDoc:IXMLDomDocument; Pen:TPen): IXMLDOMNode;
var LineControl:TLineControl;
 Attribut:IXMLDOMNode;
 SubNode:IXMLDOMNode;
begin
  Result:=XMLDoc.createElement('Pen');
  SubNode:= XMLDoc.createElement('Width');
  SubNode.text:= IntToStr(Pen.Width);
  Result.appendChild(SubNode);
  SubNode:= XMLDoc.createElement('Color');
  SubNode.text:= Format('$%x',[Pen.Color]);
  Result.appendChild(SubNode);
end;

function GenParametersNode(XMLDoc:IXMLDomDocument; Parameters:IShapeParameters): IXMLDOMNode;
var
 Attribut:IXMLDOMNode;
 SubNode:IXMLDOMNode;
 i:Integer;
begin
  Result:=XMLDoc.createElement('Parameters');
  for i:=0 to Parameters.Count-1 do
    begin
    SubNode:= XMLDoc.createElement('Param');

    Attribut:=XMLDoc.createAttribute('Num');
    Attribut.nodeValue:= IntToStr(i);
    SubNode.attributes.setNamedItem(Attribut);

    Attribut:=XMLDoc.createAttribute('NickName');
    Attribut.nodeValue:= Parameters.Item[i].NickName;
    SubNode.attributes.setNamedItem(Attribut);

    Attribut:=XMLDoc.createAttribute('Unit');
    Attribut.nodeValue:= Parameters.Item[i].UnitStr;
    SubNode.attributes.setNamedItem(Attribut);

    Attribut:=XMLDoc.createAttribute('DataType');
    if Parameters.Item[i].DataType=pdtString then  Attribut.nodeValue:='pdtString';
    if Parameters.Item[i].DataType=pdtNumeric then  Attribut.nodeValue:='pdtNumeric';
    SubNode.attributes.setNamedItem(Attribut);

    Attribut:=XMLDoc.createAttribute('Protection');
    if Parameters.Item[i].Protection=False then  Attribut.nodeValue:='False';
    if Parameters.Item[i].Protection=True then  Attribut.nodeValue:='True';
    SubNode.attributes.setNamedItem(Attribut);

    SubNode.text:= Parameters.Item[i].value;
    Result.appendChild(SubNode);
    end;
end;

function GenPictureNode(XMLDoc:IXMLDomDocument; Pic:TBitmap): IXMLDOMNode;
var LineControl:TLineControl;
 Attribut:IXMLDOMNode;
 SubNode:IXMLDOMNode;
 Stream:TMemoryStream;
 Base64Str:TStringStream;
begin
  Stream:=TMemoryStream.Create;
  Pic.SaveToStream(Stream);
  Base64Str:=TStringStream.Create('');
  Stream.Position:=0;
  EncodeStream(Stream, Base64Str);

  Result:=XMLDoc.createElement('Picture');
  Attribut:=XMLDoc.createAttribute('Size');
  Attribut.nodeValue:= Length(Base64Str.DataString);
  Result.attributes.setNamedItem(Attribut);
  SubNode:= XMLDoc.createElement('Data');
  SubNode.text:= Base64Str.DataString;
  Result.appendChild(SubNode);
  Stream.Destroy;
  Base64Str.Destroy;
end;

function GenGroup(XMLDoc:IXMLDomDocument; GroupControl:TGroupControl):IXMLDOMNode;
var ElementGroup,SubElement, PointElement, Attribut:IXMLDOMNode;
  PenElement:IXMLDOMNode;
  ParametsElement:IXMLDOMNode;
 LineControl:TLineControl;
 BezierControl:TBezierControl;
 ImageControl:TImageControl;
 SubGroupControl:TGroupControl;
i:Integer;
begin
    Result:=XMLDoc.createElement('Group');
    for i:=0 to Length(GroupControl.Group.Shapes)-1 do
       begin
       case GroupControl.Group.Shapes[i].ShapeKind of
       skUndefine: SubElement:=XMLDoc.createElement('Undefine');
       skLine:
         begin
          LineControl:= GroupControl.Group.Shapes[i].GetSelf as TLineControl;
          SubElement:=XMLDoc.createElement('Line');

          PenElement:=GenPenNode(XMLDoc, LineControl.Pen);
          SubElement.appendChild(PenElement);

          ParametsElement:=GenParametersNode(XMLDoc, LineControl.Parameters);
          SubElement.appendChild(ParametsElement);

          PointElement:=GenPointNode(XMLDoc, LineControl.Segment.P0,0);
          SubElement.appendChild(PointElement);
          PointElement:=GenPointNode(XMLDoc, LineControl.Segment.P1,1);
          SubElement.appendChild(PointElement);
         end;
       skBezier:
         begin
          BezierControl:= GroupControl.Group.Shapes[i].GetSelf as TBezierControl;
          SubElement:=XMLDoc.createElement('Bezier');

          PenElement:=GenPenNode(XMLDoc, LineControl.Pen);
          SubElement.appendChild(PenElement);

          ParametsElement:=GenParametersNode(XMLDoc, LineControl.Parameters);
          SubElement.appendChild(ParametsElement);

          PointElement:=GenPointNode(XMLDoc, BezierControl.Spline.P0,0);
          SubElement.appendChild(PointElement);
          PointElement:=GenPointNode(XMLDoc, BezierControl.Spline.P1,1);
          SubElement.appendChild(PointElement);
          PointElement:=GenPointNode(XMLDoc, BezierControl.Spline.P2,2);
          SubElement.appendChild(PointElement);
          PointElement:=GenPointNode(XMLDoc, BezierControl.Spline.P3,3);
          SubElement.appendChild(PointElement);
         end;
       skImage:
         begin
          ImageControl:= GroupControl.Group.Shapes[i].GetSelf as TImageControl;
          SubElement:=XMLDoc.createElement('Image');

          ParametsElement:=GenParametersNode(XMLDoc, LineControl.Parameters);
          SubElement.appendChild(ParametsElement);

          PointElement:=GenPointNode(XMLDoc, ImageControl.Image.Quadrilateral.P0,0);
          SubElement.appendChild(PointElement);
          PointElement:=GenPointNode(XMLDoc, ImageControl.Image.Quadrilateral.P1,1);
          SubElement.appendChild(PointElement);
          PointElement:=GenPointNode(XMLDoc, ImageControl.Image.Quadrilateral.P2,2);
          SubElement.appendChild(PointElement);
          PointElement:=GenPointNode(XMLDoc, ImageControl.Image.Quadrilateral.P3,3);
          SubElement.appendChild(PointElement);

          PointElement:=GenPictureNode(XMLDoc, ImageControl.Image.Bitmap);
          SubElement.appendChild(PointElement);
         end;
       skGroup:
         begin
          SubElement:=XMLDoc.createElement('Group');
          SubGroupControl:= GroupControl.Group.Shapes[i].GetSelf as TGroupControl;

          ParametsElement:=GenParametersNode(XMLDoc, LineControl.Parameters);
          SubElement.appendChild(ParametsElement);
          
          PointElement:=GenGroup(XMLDoc,SubGroupControl);
          SubElement.appendChild(PointElement);

         end;
       end;
       Result.appendChild(SubElement);
       end;

end;

function VFFSave(FileName:String; GroupControl:TGroupControl):boolean;
var
    XMLDoc:IXMLDomDocument;
    XMLError:IXMLDOMParseError;
    Element, SubElement:IXMLDOMNode;
    I:Integer;
begin
  XMLDoc := CoDOMDocument.Create;
  XMLDoc.Async := False;
  XMLDoc.validateOnParse:=true;
  XMLDoc.preserveWhiteSpace:=true;

  Element:=XMLDoc.createElement('Body');
  SubElement:=GenManifestNode(XMLDoc, ProgrammName, ProgrammVersion);
  Element.appendChild(SubElement);
  SubElement:=GenGroup(XMLDoc,GroupControl);
  Element.appendChild(SubElement);
  XMLDoc.appendChild(Element);
  XMLDoc.save(FileName);

  Result:=True;
end;

function ParsePoint(Node:IXMLDOMNode):TVectorReal;
var XNode,YNode:IXMLDOMNode;
begin
  Result.X:=0.0;
  Result.Y:=0.0;
  XNode:=Node.selectSingleNode('X');
  YNode:=Node.selectSingleNode('Y');
  if XNode<>nil then
     Result.X:=StrToFloatDef(XNode.text,0.0);
  if YNode<>nil then
     Result.Y:=StrToFloatDef(YNode.text,0.0);
end;

function ParsePointList(Node:IXMLDOMNode):TArrayVectorReal;
var
 p:TVectorReal;
 nodeList:IXMLDOMNodeList;
 nodePoint:IXMLDOMNode;
 i:Integer;
begin
  SetLength(Result,0);
  nodeList:=Node.selectNodes('Point');
  if nodeList<>nil then
    begin
    nodeList.reset;
    for i:=0 to nodeList.length-1 do
      begin
      nodePoint:=nodeList.item[i];
      P:=ParsePoint(nodePoint);
      SetLength(Result,Length(Result)+1);
      Result[Length(Result)-1]:=P;
      end;
    end;
end;

procedure ParsePen(Node:IXMLDOMNode; var Pen:TPen);
var WidthNode,ColorNode:IXMLDOMNode;
var PenNode:IXMLDOMNode;
begin
  PenNode:=Node.selectSingleNode('Pen');
  if PenNode<>nil then
    begin
    WidthNode:=PenNode.selectSingleNode('Width');
    ColorNode:=PenNode.selectSingleNode('Color');
    if WidthNode<>nil then
       Pen.Width:=StrToIntDef(WidthNode.text, Pen.Width);
    if ColorNode<>nil then
       Pen.Color:=StrToIntDef(ColorNode.text, Pen.Color);
    end;
end;

function ParsePicture(Node:IXMLDOMNode):TBitmap;
var Data:IXMLDOMNode;
 Stream:TMemoryStream;
 Base64Str:TStringStream;
begin
Result:=TBitmap.Create;
Result.PixelFormat:=pf32Bit;
  Data:=Node.selectSingleNode('Picture/Data');
  if Data<>nil then
    begin
    Stream:=TMemoryStream.Create;
    Base64Str:=TStringStream.Create(Data.text);
    Base64Str.Position:=0;
    DecodeStream(Base64Str,Stream);
    Stream.Position:=0;
    Result.LoadFromStream(Stream);
    Stream.Destroy;
    Base64Str.Destroy;
    end;
end;

procedure ParseParameters(Node:IXMLDOMNode; Parameters:IShapeParameters);
var
  ParameterNode:IXMLDOMNode;

  NameNode:IXMLDOMNode;
  ProtectionNode:IXMLDOMNode;
  DataTypeNode:IXMLDOMNode;
  UnitNode:IXMLDOMNode;

  DataType:TParameterDataType;
  Protection:boolean;

  SubNode:IXMLDOMNode;
  NodeList:IXMLDOMNodeList;
  i:Integer;
begin
  SubNode:=Node.selectSingleNode('Parameters');
  if SubNode<>nil then
    begin
    NodeList:=SubNode.selectNodes('Param');
    NodeList.reset;
    for i:=0 to NodeList.length-1 do
      begin
      ParameterNode:=NodeList.item[i];
      NameNode:=ParameterNode.attributes.getNamedItem('NickName');
      if NameNode.nodeValue='����������� �����������' then
        begin
        Parameters.Value[NameNode.nodeValue]:=ParameterNode.text;
        end else
        if NameNode.nodeValue='����������� �����' then
          begin
          Parameters.Value[NameNode.nodeValue]:=ParameterNode.text;
          end else
          if NameNode.nodeValue='������������' then
            begin
            Parameters.Value[NameNode.nodeValue]:=ParameterNode.text;
            end else
            begin
            ProtectionNode:=ParameterNode.attributes.getNamedItem('Protection');
            DataTypeNode:=ParameterNode.attributes.getNamedItem('DataType');
            UnitNode:=ParameterNode.attributes.getNamedItem('Unit');

            if DataTypeNode.nodeValue='pdtString' then DataType:=pdtString;
            if DataTypeNode.nodeValue='pdtNumeric' then DataType:=pdtNumeric;

            if ProtectionNode.nodeValue='False' then Protection:=False;
            if ProtectionNode.nodeValue='True' then Protection:=True;

            Parameters.New(NameNode.nodeValue,DataType,UnitNode.nodeValue,Protection);
            Parameters.Value[NameNode.nodeValue]:=ParameterNode.text;
            end;
      end;
    end;
end;

function ParseGroup(Node:IXMLDOMNode):TGroupControl;
var
 childNode:IXMLDOMNode;
 LineControl:TLineControl;
 PolyLineControl:TPolyLineControl;
 BezierControl:TBezierControl;
 ImageControl:TImageControl;
 SubGroupControl:TGroupControl;
 PointList:TArrayVectorReal;
 i,j:Integer;
begin
    Result:=TGroupControl.Create;
        if Node.hasChildNodes then
          begin
          for j:=0 to node.childNodes.length-1 do
              begin
              childNode:=node.childNodes.item[j];
              if (childNode.nodeName='Line') then
                begin
                 LineControl:=TLineControl.Create;

                 ParsePen(childNode,LineControl.Pen);

                 ParseParameters(childNode,LineControl.Parameters);

                 PointList:=ParsePointList(childNode);
                 LineControl.Segment.P0:=Point(Round(PointList[0].X),Round(PointList[0].Y));
                 LineControl.Segment.P1:=Point(Round(PointList[1].X),Round(PointList[1].Y));
                 Result.Add(LineControl);
                 SetLength(PointList,0);
                end;
              if (childNode.nodeName='PolyLine') then
                begin
                 PolyLineControl:=TPolyLineControl.Create;

                 ParsePen(childNode,PolyLineControl.Pen);

                 PointList:=ParsePointList(childNode);
                 SetLength(PolyLineControl.PolyLine,Length(PointList));
                 for i:=0 to Length(PointList)-1 do
                   PolyLineControl.PolyLine[i]:=Point(Round(PointList[i].X),Round(PointList[i].Y));

                 Result.Add(PolyLineControl);
                 SetLength(PointList,0);
                end;
              if (childNode.nodeName='Bezier') then
                begin
                 BezierControl:=TBezierControl.Create;

                 ParsePen(childNode,BezierControl.Pen);

                 PointList:=ParsePointList(childNode);
                 BezierControl.Spline.P0:=Point(Round(PointList[0].X),Round(PointList[0].Y));
                 BezierControl.Spline.P1:=Point(Round(PointList[1].X),Round(PointList[1].Y));
                 BezierControl.Spline.P2:=Point(Round(PointList[2].X),Round(PointList[2].Y));
                 BezierControl.Spline.P3:=Point(Round(PointList[3].X),Round(PointList[3].Y));
                 Result.Add(BezierControl);
                 SetLength(PointList,0);
                end;
              if (childNode.nodeName='Image') then
                begin
                 ImageControl:=TImageControl.create(ParsePicture(childNode));
                 PointList:=ParsePointList(childNode);
                 if Length(PointList)<>0 then
                    begin
                    ImageControl.Image.Quadrilateral.P0:=Point(Round(PointList[0].X),Round(PointList[0].Y));
                    ImageControl.Image.Quadrilateral.P1:=Point(Round(PointList[1].X),Round(PointList[1].Y));
                    ImageControl.Image.Quadrilateral.P2:=Point(Round(PointList[2].X),Round(PointList[2].Y));
                    ImageControl.Image.Quadrilateral.P3:=Point(Round(PointList[3].X),Round(PointList[3].Y));
                    SetLength(PointList,0);
                    end;
                  Result.Add(ImageControl);
                end;
              if (childNode.nodeName='Group') then
                begin
                SubGroupControl:=ParseGroup(childNode);
                Result.Add(SubGroupControl);
                end;
              end;
          end;

end;

function VFFLoad(FileName:String; var GroupControl:TGroupControl):boolean;
var
    XMLDoc:IXMLDomDocument;
    XMLError:IXMLDOMParseError;
    Nodes, nodeList:IXMLDOMNodeList;
    Node, childNode:IXMLDOMNode;
    I,j:Integer;
begin
Result:=False;
  if FileExists(FileName) then
     begin
    XMLDoc := CoDOMDocument.Create;
    XMLDoc.Async := False;
    XMLDoc.validateOnParse:=true;
    XMLDoc.preserveWhiteSpace:=true;
    XMLDoc.Load(FileName);

    XMLError := XMLDoc.ParseError;


    If XMLError.ErrorCode <> 0 Then
      begin
      Application.MessageBox(PAnsiChar('������ � ��������� �����:"'+FileName+'";'+XMLError.reason+';'+IntToStr(XMLError.line)), '������');   //
      exit;
      end;

    Node:=XMLDoc.selectSingleNode('Body/Group');
    if GroupControl<>nil then GroupControl:=nil;
    GroupControl:=ParseGroup(Node);

    result:=True;
    end;
end;

end.
